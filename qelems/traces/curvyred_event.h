#ifndef	__CURVYRED_EVENT__
#define	__CURVYRED_EVENT__

#ifdef IN_IDE_PARSER
#define WITH_TRACES 2
#endif

#ifdef WITH_TRACES

#include <stdint.h>
#include "trace_event_queue.h"

enum CURVYRED_EVENT {CURVYRED_PKTS_TAILDROP, CURVYRED_DEQUEUE};

typedef struct curvyred_event
{
  uint64_t now;
  uint32_t event_type;
  uint32_t aqm_drops_taildrops;
  uint64_t curr_sojourn;
  double avg_sojourn;
} curvyred_event_t;


static inline void curvyred_on_pkts_taildrop ( event_log_queue_t* queue, uint64_t now, unsigned num_taildrop )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    curvyred_event_t* event = & ( ( ( curvyred_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = CURVYRED_PKTS_TAILDROP;
    event->now = now;
    event->aqm_drops_taildrops = num_taildrop;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}



static inline void curvyred_on_pkts_dequeued ( event_log_queue_t* queue, uint64_t now, uint64_t curr_sojourn, double avg_sojourn, unsigned is_dropped)
{
  if ( ( queue->writer_index < queue->size ) )
  {
    curvyred_event_t* event = & ( ( ( curvyred_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = CURVYRED_DEQUEUE;
    event->now = now;
    event->aqm_drops_taildrops = is_dropped;
    event->curr_sojourn = curr_sojourn;
    event->avg_sojourn = avg_sojourn;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

#else

#define curvyred_on_pkts_taildrop(queue,now,taildrops)
#define curvyred_on_pkts_dequeued(queue,now,curr_sojourn,avg_sojourn,is_dropped)


#endif // ifdef WITH_TRACES


#endif // ifndef __QTRL_TAILDROP_LOG__



