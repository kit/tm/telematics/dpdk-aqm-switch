#ifndef	__TAILDROP_EVENT__
#define	__TAILDROP_EVENT__

#ifdef IN_IDE_PARSER
#define WITH_TRACES 2
#define TRACES_QSAMPLING 8
#endif

#ifdef WITH_TRACES

#include <stdint.h>
#include "trace_event_queue.h"

enum TAILDROP_EVENT {TAILDROP_PKT_DROP, TAILDROP_PKT_DEQUEUE};

typedef struct taildrop_event {
    uint64_t now;
    uint64_t last_sojourn;
    uint32_t event_type;
    uint32_t qlen_pkts;
    uint32_t qlen_bytes;
    uint32_t taildrops;
} taildrop_event_t;

static inline void taildrop_on_pkts_drop ( event_log_queue_t* queue, uint64_t now, uint32_t num_tail_drops )
{
    if ( ( queue->writer_index < queue->size ) ) {
        taildrop_event_t* event = & ( ( ( taildrop_event_t* ) queue->events ) [queue->writer_index] );
        event->event_type = TAILDROP_PKT_DROP;
        event->now = now;
        event->taildrops = num_tail_drops;
        COMPILER_BARRIER();
        queue->writer_index++;
    }
}

static inline void taildrop_on_pkts_dequeue ( event_log_queue_t* queue, uint64_t now, uint32_t qlen_pkts, uint32_t qlen_bytes, uint32_t num_deq, uint64_t last_sojourn)
{
    if (num_deq > 0 && ( queue->writer_index < queue->size ) ) {
      queue->sampling_index++;
      if( (queue->sampling_index & (TRACES_QSAMPLING-1)) == 0){
        taildrop_event_t* event = & ( ( ( taildrop_event_t* ) queue->events ) [queue->writer_index] );
        event->event_type = TAILDROP_PKT_DEQUEUE;
        event->now = now;
	event->last_sojourn = last_sojourn;
        event->qlen_pkts = qlen_pkts;
        event->qlen_bytes = qlen_bytes;
        event->taildrops = 0;
        COMPILER_BARRIER();
        queue->writer_index++;
      }
    }
}

#else

#define taildrop_on_pkts_drop(queue, now, num_tail_drops)
#define taildrop_on_pkts_dequeue(queue, now, qlen_pkts, qlen_bytes, num_deq)


#endif // ifdef WITH_TRACES


#endif // ifndef __QTRL_TAILDROP_LOG__



