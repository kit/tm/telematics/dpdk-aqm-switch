#ifndef	__QTRACE_EVENT_QUEUE__
#define	__QTRACE_EVENT_QUEUE__

#include <sys/queue.h>

#define COMPILER_BARRIER() asm volatile("" ::: "memory")
#define CACHE_LINE_SIZE 64

#ifndef NULL
#define NULL 0
#endif

typedef struct __attribute__ ( ( aligned ( CACHE_LINE_SIZE ) ) )
{
    volatile long unsigned writer_index;
    long unsigned size;
    long unsigned sampling_index;
    void* events;
}
event_log_queue_t;


struct __trace_entry;
typedef void ( *logger_func ) ( struct __trace_entry* queue );

typedef struct __trace_entry {
    LIST_ENTRY ( __trace_entry ) l_entry;
    logger_func logger;
    void *fd;
    void *fd_bin;
    long unsigned reader_index;
    event_log_queue_t* queue;
} trace_event_queue_t;

typedef LIST_HEAD ( , __trace_entry ) trace_event_queues_t;

extern trace_event_queues_t trace_event_queues_list;

void trace_event_init ( void );
event_log_queue_t* trace_event_queue_add ( const char* log_file, long unsigned int num_entries, long unsigned int size_of_entry, logger_func logger );

static inline int are_traces_available(void)
{
    return ! LIST_EMPTY ( &trace_event_queues_list );
}

//------------------------------------------------------------------------------
// AQM specific loggers
//------------------------------------------------------------------------------

void taildrop_event_log ( trace_event_queue_t* queue );
void codel_event_log ( trace_event_queue_t* queue );
void fq_codel_event_log ( trace_event_queue_t* queue );
void pie_event_log ( trace_event_queue_t* queue );
void red_event_log ( trace_event_queue_t* queue );
void gsp_event_log ( trace_event_queue_t* queue );
void curvyred_event_log ( trace_event_queue_t* queue );

#endif // ifndef __QTRL_CODEL_LOG__

