#ifndef __PIE_EVENT__
#define __PIE_EVENT__

#ifdef IN_IDE_PARSER
#define WITH_TRACES 2
#define TRACES_QSAMPLING 8
#endif

#ifdef WITH_TRACES

#include <stdint.h>
#include "trace_event_queue.h"

enum PIE_EVENT {PIE_DROP, PIE_QUEUE_SOJOURN, PIE_BALLOW_RESET, PIE_UPDATE, PIE_DRAIN_RATE_EST, PIE_MEASUREMENT};
// enum PKT_ENQ { PKT_ENQUEUED = 0, PKT_AQM_DROPPED = 1, PKT_TAILDROPPED = 2};

typedef struct pie_event
{
  uint64_t now;
  uint64_t qlen_bytes__sojourn;
  uint64_t burstallow_taildrops; // num tail drops, burst_allow
  uint32_t event_type;
  uint32_t aqmdrops_measurement; // num_aqm_drops // enter/exit measurement
  double avgdrainrate_p; // avg_drain_rate, p

//     uint32_t qlen_bytes;
  double curr_drainrate_delay; // curr_drain_rate, curr_delay
} pie_event_t;

// TODO: fixme
// static inline void pie_on_pkts_enqueue ( event_log_queue_t* queue, uint64_t now, uint32_t qlen_bytes, uint32_t num_aqm_drops, uint32_t num_tail_drops )
// {
// #if WITH_TRACES == 2
//   if ( ( queue->writer_index < queue->size ) )
//   {
//     queue->sampling_index++;
//     if ( ( queue->sampling_index & ( TRACES_QSAMPLING-1 ) ) == 0 )
//     {
//       pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
//       event->event_type = PIE_PKT_ENQUEUE_LOG;
//       event->now = now;
//       event->qlen_bytes = qlen_bytes;
//       event->aqmdrops_measurement = num_aqm_drops;
//       event->burstallow_taildrops = num_tail_drops;
//       COMPILER_BARRIER();
//       queue->writer_index++;
//     }
//   }
// #endif
// }

static inline void pie_sojourn_on_pkts_enqueue ( event_log_queue_t* queue, uint64_t now, uint32_t num_aqm_drops, uint32_t num_tail_drops )
{
  if ( ( num_aqm_drops > 0 || num_tail_drops > 0 ) && ( queue->writer_index < queue->size ) )
  {
    pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = PIE_DROP;
    event->now = now;
    event->aqmdrops_measurement = num_aqm_drops;
    event->burstallow_taildrops = num_tail_drops;
    COMPILER_BARRIER();
    queue->writer_index++;
  }

}

static inline void pie_sojourn_on_pkts_dequeue ( event_log_queue_t* queue, uint64_t now, uint64_t sojourn )
{
  if ( queue->writer_index < queue->size )
  {
    queue->sampling_index++;
    if ( ( queue->sampling_index & ( TRACES_QSAMPLING-1 ) ) == 0 )
    {
      pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
      event->event_type = PIE_QUEUE_SOJOURN;
      event->now = now;
      event->qlen_bytes__sojourn = sojourn;
      COMPILER_BARRIER();
      queue->writer_index++;
    }
  }
}


static inline void pie_on_update ( event_log_queue_t* queue, uint64_t now, double curr_delay, double p, uint64_t burst_allow )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = PIE_UPDATE;
    event->now = now;
    event->curr_drainrate_delay = curr_delay;
    event->avgdrainrate_p = p;
    event->burstallow_taildrops = burst_allow;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

static inline void pie_on_ballow_reset ( event_log_queue_t* queue, uint64_t now, uint64_t burst_allow )
{
  if ( ( queue->writer_index < queue->size ) )
  {
    pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
    event->event_type = PIE_BALLOW_RESET;
    event->now = now;
    event->burstallow_taildrops = burst_allow;
    COMPILER_BARRIER();
    queue->writer_index++;
  }
}

static inline void pie_on_enter_measurement ( event_log_queue_t* queue, uint64_t now, uint64_t burst_allow )
{
//     if ( ( queue->writer_index < queue->size ) ) {
//         pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
//         event->event_type = PIE_MEASUREMENT;
//         event->now = now;
//         event->aqmdrops_measurement = 1;
//         event->burstallow_taildrops = burst_allow;
//         COMPILER_BARRIER();
//         queue->writer_index++;
//     }
}

static inline void pie_on_exit_measurement ( event_log_queue_t* queue, uint64_t now, uint64_t burst_allow )
{
//     if ( ( queue->writer_index < queue->size ) ) {
//         pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
//         event->event_type = PIE_MEASUREMENT;
//         event->now = now;
//         event->aqmdrops_measurement = 0;
//  event->burstallow_taildrops = burst_allow;
//         COMPILER_BARRIER();
//         queue->writer_index++;
//     }
}

static inline void pie_on_new_drain_rate_estimation ( event_log_queue_t* queue, uint64_t now,
    double curr_drain_rate, double avg_drain_rate, uint64_t burst_allow )
{
//     if ( ( queue->writer_index < queue->size ) ) {
//         pie_event_t* event = & ( ( ( pie_event_t* ) queue->events ) [queue->writer_index] );
//         event->event_type = PIE_DRAIN_RATE_EST;
//         event->now = now;
//         event->curr_drainrate_delay = curr_drain_rate;
//         event->avgdrainrate_p = avg_drain_rate;
//         event->burstallow_taildrops = burst_allow;
//         COMPILER_BARRIER();
//         queue->writer_index++;
//     }
}


#else

#define pie_on_pkts_enqueue(queue, now, qlen_bytes, num_aqm_drops, num_tail_drops)
#define pie_on_pkts_dequeue(queue, now, qlen_bytes)
#define pie_on_update(queue, now, qlen_bytes, curr_delay, p, burst_allow)
#define pie_on_enter_measurement(queue, now, burst_allow)
#define pie_on_exit_measurement(queue, now, burst_allow)
#define pie_on_new_drain_rate_estimation(queue, now, curr_drain_rate, avg_drain_rate, burst_allow)


#endif // ifdef WITH_TRACES


#endif // ifndef __QTRL_PIE_LOG__



