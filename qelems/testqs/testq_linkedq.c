#include "testqs.h"
#include "../../rte_include.h"
#include "../qlog.h"
#include "macros.h"
#define NO_BYTES_COUNTER
#include "../qdt/linkedq.h"

#include "testq_events.h"
#include "testq_log.h"

#ifdef IN_IDE_PARSER
#define MAX_BURST 32
#endif

static qelem_ops_t testq_linkedq_ops;

typedef struct
{
    qelem_t qelem;

#ifdef WITH_TRACES
    event_log_queue_t* trace_queue;
#endif

    linkedq_t queue;
} testq_linkedq_t __rte_cache_aligned;


//------------------------------------------------------------------------------------------------------

static
qelem_t* testq_linkedq_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
    LOG_INIT ( "allocating testq_linkedq for port %d \n", common_params->port_id );

    TQPARAMSCAST();
    testq_linkedq_t* testq = 0;

    TQZMALLOC ( testq, testq_linkedq_t, 0 );
    qelem_init ( ( qelem_t* ) testq, common_params, &testq_linkedq_ops );
    linkedq_init ( &testq->queue, params->qlen_pkts );

    LOG_PARAM(params->qlen_pkts, "%u");

#ifdef WITH_TRACES
    testq->trace_queue = common_params->trace_queue;
#endif

    return ( qelem_t* ) testq;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned testq_linkedq_enqueue_burst ( testq_linkedq_t *testq, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    unsigned num_enqueued = linkedq_enqueue_burst(&testq->queue, pkts, num_pkts);

    // drop packets which didn't fit in
    unsigned pkt_num;
    if ( unlikely ( num_enqueued < num_pkts ) )
    {
        for ( pkt_num = num_enqueued ; pkt_num < num_pkts; pkt_num++ )
        {
            rte_pktmbuf_free ( pkts[pkt_num] );
        };
    }

    return num_enqueued;
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

// static
// void testq_linkedq_tx_flush ( qelem_t *qelem )
// {
//   TQCAST ( linkedq );
// 
//   struct rte_mbuf** burst = 0;
//   unsigned num_pkts = linkedq_peek_burst (&testq->queue, &burst);
// 
//   if ( likely ( num_pkts > 0 ) )
//   {
//     unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, burst, num_pkts );
//     if ( num_tx > 0 )
//     {
//       linkedq_dequeue_peeked_burst(&testq->queue, burst, num_tx);
//     }
//   }
// }

static
void testq_linkedq_tx_flush ( qelem_t *qelem )
{
    TQCAST ( linkedq );

    unsigned pkt_num;
    for(pkt_num = 0; pkt_num < testq->qelem.min_send_burst; pkt_num++) {
        struct rte_mbuf* pkt = linkedq_head(&testq->queue);
        if(pkt) {
//             LOG_VAR(pkt, "%p");
            unsigned num_tx = rte_eth_tx_burst ( qelem->port_id, qelem->queue_id, &pkt, 1 );
            if ( num_tx == 1 )
            {
                linkedq_dequeue_head(&testq->queue);
            }
        }
    }
}

static
void testq_linkedq_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    TQCAST ( linkedq );

    // enqueue packets
    testq_linkedq_enqueue_burst ( testq, pkts, num_pkts );
//     LOG_VAR(num_pkts, "%u");

    // if there is at least burst packets in the queue, transmit packets
    if ( likely ( linkedq_count(&testq->queue) >= testq->qelem.min_send_burst ) )
    {
        testq_linkedq_tx_flush ( qelem );
    }
}

static
void testq_linkedq_free ( qelem_t* qelem )
{
    rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* testq_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    return trace_event_queue_add ( "testq", common_params->trace_queue_size, sizeof ( testq_event_t ), testq_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t testq_linkedq_factory =
{
    .qelem_alloc = testq_linkedq_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = testq_trace_queue_alloc,
#endif
};

static qelem_ops_t testq_linkedq_ops =
{
    .qelem_tx_burst = testq_linkedq_tx_burst,
    .qelem_tx_flush = testq_linkedq_tx_flush,
    .qelem_free = testq_linkedq_free,
};

