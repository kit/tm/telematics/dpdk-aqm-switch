#define TQPARAMSCAST() const testq_params_t* params = (const testq_params_t*) qelem_params

#define TQZMALLOC(VAR, TYPE, EXTRA_SIZE) \
do { \
  VAR = (TYPE*) rte_zmalloc(#TYPE, sizeof(TYPE) + EXTRA_SIZE, RTE_CACHE_LINE_SIZE); \
  if(!VAR) { rte_panic("cannot allocate memory for " #TYPE); } \
} while(0);

#define TQCAST(QNAME) testq_ ## QNAME ## _t* testq = (testq_ ## QNAME ## _t*) qelem
