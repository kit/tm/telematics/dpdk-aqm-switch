#ifndef	__QM_APPROX_RECIPROCAL_SQRT__
#define	__QM_APPROX_RECIPROCAL_SQRT__

#include "qm_types.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

// newton's method for approximating reciprocal sqrt
// http://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Iterative_methods_for_reciprocal_square_roots
// code copied from linux's codel code

static inline void approx_rec_sqrt_reset ( Q_0_32* approx_sqrt )
{
    *approx_sqrt = ~0; // ~0 = 111111 = 0.99999 in our notation
};

static inline void approx_rec_sqrt_next ( Q_0_32* approx_sqrt, uint32_t value )
{
    // approx_sqrt_next = prev/2 * (3 - value * prev^2);
    // prev is Q0.32 integer

    // value table // TODO: I hope gcc computes this
    // TODO: check performance

    if ( value == 1 ) {
        approx_rec_sqrt_reset ( approx_sqrt );
    } else if ( value == 2 ) {
        *approx_sqrt = ( uint32_t ) ( ( 0.70710678118 * ( 1llu << 32 ) ) );
        return;
    } else if ( value == 3 ) {
        *approx_sqrt = ( uint32_t ) ( ( 0.57735026919 * ( 1llu << 32 ) ) );
        return;
    } else if ( value == 4 ) {
        *approx_sqrt = ( uint32_t ) ( ( 0.5* ( 1llu << 32 ) ) );
        return;
    } else if ( value == 5 ) {
        *approx_sqrt = ( uint32_t ) ( ( 0.4472135955* ( 1llu << 32 ) ) );
        return;
    }

    uint32_t prev = *approx_sqrt;
    uint32_t prev_squared = q_0_32_mul ( prev,prev );

    uint64_t bracket = ( 3LL << 32 ) - ( ( uint64_t ) value * prev_squared );

    bracket >>= 2; /* avoid overflow in following multiply */
    uint64_t next = ( ( prev/2 ) * bracket ) >> ( 32 - 2 );

    *approx_sqrt = next;
};

static inline uint32_t reciprocal_scale ( uint32_t val, uint32_t scale )
{
    return ( uint32_t ) ( ( ( uint64_t ) val * scale ) >> 32 );
}




#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
