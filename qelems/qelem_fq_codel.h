#ifndef	__QELEM_FQ_CODEL__
#define	__QELEM_FQ_CODEL__

#include "qelem.h"
#include "qls.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

typedef struct
{
  unsigned num_subqueues;
  unsigned pkt_hard_limit;
  double interval_sec;
  double target_sec;
  unsigned mtu;
  
  void* qls_params; // parameters for the classifier
  qls_create_op qls_create; // classifier initializer
} qelem_fq_codel_params_t;

static const qelem_fq_codel_params_t fq_codel_default_params = {
  .num_subqueues = 1024,
  .pkt_hard_limit = 10240,
  .interval_sec = 20e-3, // 20ms
  .target_sec = 1e-3, // 1 ms = 5% of 20ms
  .mtu = 1522,
};

extern qelem_factory_t qelem_fq_codel_factory;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
