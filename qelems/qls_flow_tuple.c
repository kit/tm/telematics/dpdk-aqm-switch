#include "qls_flow_tuple.h"
#include "../rte_include.h"

static uint32_t qft_hash_flow_classify ( qls_t* qls, struct rte_mbuf* pkt )
{
  uint32_t hash = 0;
  uint32_t data;

  struct ether_hdr* eth_hdr = rte_pktmbuf_mtod ( pkt, struct ether_hdr* );

  const uint16_t swapped_ETHER_TYPE_IPv4 = rte_constant_bswap16 ( ETHER_TYPE_IPv4 );
  const uint16_t swapped_ETHER_TYPE_IPv6 = rte_constant_bswap16 ( ETHER_TYPE_IPv6 );

  if ( eth_hdr->ether_type == swapped_ETHER_TYPE_IPv4 )
  {
    // set ip4 flow key: <src_ip,src_port,dst_ip,dst_port,proto>

    struct ipv4_hdr *ip_hdr = ( struct ipv4_hdr * ) &eth_hdr[1];
    hash = rte_hash_crc ( &ip_hdr->src_addr, 4, hash );
    hash = rte_hash_crc ( &ip_hdr->dst_addr, 4, hash );
    hash = rte_hash_crc ( &ip_hdr->next_proto_id, 1, hash );

    switch ( ip_hdr->next_proto_id )
    {
    case IPPROTO_TCP:
    {
      struct tcp_hdr* tcp_hdr = ( struct tcp_hdr* ) &ip_hdr[1];
      hash = rte_hash_crc ( &tcp_hdr->src_port, 4, hash ); // will hash src and immediately following dst ports
      break;
    }
    case IPPROTO_UDP:
    {
      struct udp_hdr* udp_hdr = ( struct udp_hdr* ) &ip_hdr[1];
      hash = rte_hash_crc ( &udp_hdr->src_port, 4, hash ); // will hash src and immediately following dst ports
      break;
    }
    }
  }
  else if ( eth_hdr->ether_type == swapped_ETHER_TYPE_IPv6 )
  {
    // set ip6 flow key: <src_ip,dst_ip,flow_label>

    struct ipv6_hdr *ip_hdr = ( struct ipv6_hdr * ) &eth_hdr[1];
    hash = rte_hash_crc ( &ip_hdr->src_addr, sizeof ( ip_hdr->src_addr ) * 2, hash );
    data = ( ip_hdr->vtc_flow & 0x000FFFFF );
    hash = rte_hash_crc ( &data, 3, hash );
  }
  else
  {
    hash = 0;
  }

  return hash;
};

static uint32_t qft_rss_classify ( qls_t* qls, struct rte_mbuf* pkt )
{
  // rss classifier is stateless -> qls is ignored
  return pkt->hash.rss;
};

qls_t* qls_flow_tuple_hash_create(void* qls_params)
{
  qls_t* qls = rte_zmalloc(0, sizeof(qls_t), 0);
  qls->qlassify = qft_hash_flow_classify;
  return qls;
};

qls_t* qls_flow_tuple_rss_create(void* qls_params)
{
  qls_t* qls = rte_zmalloc(0, sizeof(qls_t), 0);
  qls->qlassify = qft_rss_classify;
  return qls;
};
