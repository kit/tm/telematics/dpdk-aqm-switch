#ifndef	__QELEM_TYPES__
#define	__QELEM_TYPES__

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */
  
#define BURST_SIZE 32 // TODO - compile time params  
  
typedef enum __bool {FALSE = 0, TRUE = 1} boolean_t;  
typedef enum __res {FAILURE = 0, SUCCESS = 1} qres_t;  
typedef uint64_t tsc_cycles_t;
typedef double seconds_t;



#ifdef __cplusplus
}
#endif /* __cplusplus */  

#endif