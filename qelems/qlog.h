#ifndef	QELEM_QLOG
#define	QELEM_QLOG

//TODO ifdef debug
#include "macros/qdbg.h"

#define LOG_INIT(...) RTE_LOG(INFO, USER1, __VA_ARGS__);
#define LOG_PARAM(var, pfarg) RTE_LOG(DEBUG, USER1, #var " = " pfarg "\n" , var);

#endif  