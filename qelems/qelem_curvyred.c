#include "qelems.h"
#include "../rte_include.h"
#include "qlog.h"
#include "macros/common.h"
#include "qmath/fastrand.h"
#include <math.h>

#define NO_BYTES_COUNTER
#include "qdt/lolliq.h"

#include "traces/curvyred_event.h"
// #include "makeconf.h"

// #ifdef WITH_ETH_OVERHEAD
// #define ETHERNET_OVERHEAD (4 + 4 + 8 + 12) // vlan tag + crc + (preamble+sof) + (ifs)
// #else
// #define ETHERNET_OVERHEAD 0
// #endif


static qelem_ops_t qelem_curvyred_ops;

typedef struct __qelem_curvyred
{
  qelem_t qelem;

  // params:
  double ewma_weight;
  double D_q;

  // state:
  double avg_qsojourn;

  rand_state_t rand_state;

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif

  lolliqueue_t queue;
} qelem_curvyred_t __rte_cache_aligned;

typedef struct
{
  tsc_cycles_t timestamp;
} pkt_metadata_t;

//------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_curvyred_alloc ( const  qelem_params_t* common_params, const void* qelem_params )
{
  LOG_INIT ( "allocating qelem_curvyred for port %d \n", common_params->port_id );

//   QPARAMSCAST ( curvyred );
  qelem_curvyred_t* curvyred = 0;

  unsigned qlen = 4096;
  
  size_t queue_mem_size = lolliq_entries_sizeof ( qlen );
  QZMALLOC ( curvyred, qelem_curvyred_t, queue_mem_size );
  qelem_init ( ( qelem_t* ) curvyred, common_params, &qelem_curvyred_ops );
  lolliq_init ( &curvyred->queue, qlen );

  // in DCTH paper f = 5 => ewma_weight = 1 / 2^5
  curvyred->ewma_weight = 1.0 / ( 1 << 5 );
  LOG_PARAM ( curvyred->ewma_weight, "%f" );

  // in curvy-red paper -> D_q is so that the queue passes 2% at 20ms point
  // convert 20ms in tsc_cycles_t
  double cycles20ms = 0.020  * rte_get_tsc_hz();
  // D_q in cycles is then
  curvyred->D_q = cycles20ms / sqrt ( 0.02 );

  fast_srand ( &curvyred->rand_state, rte_get_tsc_cycles() );


// TODO init rand

#ifdef WITH_TRACES
  curvyred->trace_queue = common_params->trace_queue;
#endif

  return ( qelem_t* ) curvyred;
};

//------------------------------------------------------------------------------------------------------
// helper opts
//------------------------------------------------------------------------------------------------------

static inline unsigned curvyred_enqueue_burst ( qelem_curvyred_t* curvyred, struct rte_mbuf **pkts,  uint32_t num_pkts, tsc_cycles_t now )
{
  unsigned num_enq = lolliq_enqueue_burst ( &curvyred->queue, pkts, num_pkts );

  unsigned pkt_num;
  for ( pkt_num = 0 ; pkt_num < num_enq; pkt_num++ )
  {
    pkt_metadata_t* metadata = ( pkt_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num] , 0 );
    metadata->timestamp = now;
  };

  if ( unlikely ( num_enq < num_pkts ) )
  {
    for ( pkt_num = num_enq; pkt_num < num_pkts; pkt_num++ )
    {
      struct rte_mbuf* pkt = pkts[pkt_num];
      rte_pktmbuf_free ( pkt );
    };
    
    curvyred_on_pkts_taildrop(curvyred->trace_queue, now, num_pkts-num_enq);
  }

  return num_enq;
}

static inline void curvyred_send_burst_per_pkt ( qelem_curvyred_t* curvyred, tsc_cycles_t now )
{
  unsigned pktnum;
  for ( pktnum = 0; pktnum < MAX_BURST; pktnum++ )
  {
    struct rte_mbuf* pkt = lolliq_head ( &curvyred->queue );
    if ( !pkt ) break;

    pkt_metadata_t* metadata = ( pkt_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkt, 0 );
    tsc_cycles_t sojourn_time = now - metadata->timestamp;

    double avg_sojourn;

    // 1 ewma for queueing delay
    avg_sojourn = curvyred->avg_qsojourn * ( 1 - curvyred->ewma_weight ) + curvyred->ewma_weight * sojourn_time;

    // drop pkt with probability p
    double scaled_sojourn = avg_sojourn / curvyred->D_q;

    // check drop probability
    unsigned u;
    boolean_t drop = TRUE;
    for ( u = 0; u < 2; u++ )
    {
      unsigned rand0 = fast_rand(&curvyred->rand_state);
      double rand = ((double) rand0 / (double) 0xffffffff); // convert rand to [0;1]
      if ( ! ( scaled_sojourn > rand ) )
      {
        drop = FALSE;
        break;
      }
    }

    // pkts should be dropped drop it else return it
    if ( drop )
    {
      curvyred->avg_qsojourn = avg_sojourn;
      lolliq_drop_head(&curvyred->queue);
      curvyred_on_pkts_dequeued(curvyred->trace_queue, now, sojourn_time, curvyred->avg_qsojourn, 1);
    }
    else
    {
      // try to enqueue packet, if successful update state,
      // if not, break
      unsigned num_tx = rte_eth_tx_burst ( curvyred->qelem.port_id, curvyred->qelem.queue_id, &pkt, 1 );
      if ( num_tx == 1 )
      {
        curvyred->avg_qsojourn = avg_sojourn;
        lolliq_dequeue_head ( &curvyred->queue );
	curvyred_on_pkts_dequeued(curvyred->trace_queue, now, sojourn_time, curvyred->avg_qsojourn, 0);
      }
      else
      {
        break;
      }
    }
  }
}

//------------------------------------------------------------------------------------------------------
// default opts
//------------------------------------------------------------------------------------------------------

static
void qelem_curvyred_tx_flush ( qelem_t *qelem )
{
  QCAST ( curvyred );

  tsc_cycles_t now = rte_get_tsc_cycles();
  curvyred_send_burst_per_pkt ( curvyred, now );
}

static
void qelem_curvyred_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
  QCAST ( curvyred );
  tsc_cycles_t now = rte_get_tsc_cycles();

  // enqueue packets
  curvyred_enqueue_burst ( curvyred, pkts, num_pkts, now );

  // if there is at least burst packets in the queue, transmit packets
  if ( likely ( lolliq_count ( &curvyred->queue ) >= curvyred->qelem.min_send_burst ) )
  {
    curvyred_send_burst_per_pkt ( curvyred, now );
  }
}

static
void qelem_curvyred_free ( qelem_t* qelem )
{
  rte_free ( qelem );
};

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* curvyred_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
  LOG_LINE();
  return trace_event_queue_add ( "curvyred", common_params->trace_queue_size, sizeof ( curvyred_event_t ), curvyred_event_log );
}
#endif

//--------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_curvyred_factory =
{
  .qelem_alloc = qelem_curvyred_alloc,
#ifdef WITH_TRACES
  .trace_queue_alloc = curvyred_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_curvyred_ops =
{
  .qelem_tx_burst = qelem_curvyred_tx_burst,
  .qelem_tx_flush = qelem_curvyred_tx_flush,
  .qelem_free = qelem_curvyred_free,

//   .enqueue_burst = qelem_curvyred_enqueue_burst,
//   .peek_burst = qelem_curvyred_peek_burst,
//   .dequeue_peeked_burst = qelem_curvyred_dequeue_peeked_burst,
//   .queue_len_bytes = qelem_curvyred_queue_len_bytes,
//   .queue_len_pkts = qelem_curvyred_queue_len_pkts,
};

