#ifndef	__QDT_LOLLIQ__
#define	__QDT_LOLLIQ__

#include "../../rte_include.h"
#include "../qelem_types.h"

#ifdef IN_IDE_PARSER
#define MAX_BURST 32
#endif

#ifndef MAX_BURST
#error please define MAX_BURST
#endif

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @file
 *
 * lolipop circular buffer for packets
 *
 * WARNING: in the code of every driver the packets are freed only after they are indicated as sent and the space must be overwritten with
 * new packet. this means, that the packets that were enqueued for transmission during one call to rte_eth_tx_burst will never be freed before
 * the next call to rte_eth_tx_burst. we rely on this behavior and update byte-count on packets that are already enqueued for transmission.
 */

typedef struct {
    uint32_t tail; // last empty location (this is fifo -> write to tail, read from head)
    uint32_t head; // first filled location
    uint32_t peek_head;

//     uint32_t burst_size;
    uint32_t stick_index;

    uint32_t qlen_bytes;
    uint32_t ring_mask;

    struct rte_mbuf** lollipop_stick;
    struct rte_mbuf** ring;
} lolliqueue_t;

static size_t lolliq_entries_sizeof ( unsigned num_entries);
static void lolliq_init ( lolliqueue_t* lolliq, unsigned queue_len);

static unsigned lolliq_qlen_bytes ( lolliqueue_t* lolliq );
static unsigned lolliq_count ( lolliqueue_t* lolliq );
// static unsigned lolliq_is_burst_enqueued ( lolliqueue_t* lolliq );
static uint32_t lolliq_free_count ( lolliqueue_t* lolliq );

static unsigned int lolliq_enqueue_pkt ( lolliqueue_t* lolliq, struct rte_mbuf* pkt );
static unsigned int lolliq_enqueue_burst ( lolliqueue_t* lolliq, struct rte_mbuf** pkts, uint32_t num_pkts );
static unsigned lolliq_peek_burst ( lolliqueue_t* lolliq, struct rte_mbuf*** pkts );
static void lolliq_dequeue_peeked_burst ( lolliqueue_t* lolliq, struct rte_mbuf** pkts, unsigned num_pkts );

static struct rte_mbuf* lolliq_head ( lolliqueue_t* lolliq );
static void lolliq_drop_head ( lolliqueue_t* lolliq );
static void lolliq_dequeue_head ( lolliqueue_t* lolliq );

//---------------------------------------------------------------------------------------------------------------------------

#define RING_IDX(index) ((index) & lolliq->ring_mask)

size_t lolliq_entries_sizeof ( unsigned int num_entries)
{
    return ( num_entries + MAX_BURST ) *  sizeof ( struct rte_mbuf* ) ;
}

void lolliq_init ( lolliqueue_t* lolliq, unsigned int queue_len)
{
    // assume that tsq is zmalloced
    lolliq->tail = 0;
    lolliq->head = queue_len - 1;
    lolliq->ring_mask = queue_len - 1;

//     lolliq->burst_size = burst_size;
    lolliq->stick_index = 0;

    lolliq->lollipop_stick = ( struct rte_mbuf** ) &lolliq[1];
    lolliq->ring = &lolliq->lollipop_stick[MAX_BURST];
};

unsigned int lolliq_qlen_bytes ( lolliqueue_t* lolliq )
{
    return lolliq->qlen_bytes;
}

unsigned int lolliq_count ( lolliqueue_t* lolliq )
{
    return RING_IDX ( lolliq->tail - ( lolliq->head + 1 ) );
}

// unsigned int lolliq_is_burst_enqueued ( lolliqueue_t* lolliq )
// {
//     return lolliq_count ( lolliq ) >= lolliq->burst_size;
// }

uint32_t lolliq_free_count ( lolliqueue_t* lolliq )
{
    return  RING_IDX ( lolliq->head - lolliq->tail );
}

unsigned int lolliq_enqueue_pkt ( lolliqueue_t* lolliq, struct rte_mbuf* pkt )
{
    if ( lolliq_free_count ( lolliq ) == 0 ) {
        return 0;
    }

    // append packet to the tail
    lolliq->ring[lolliq->tail] = pkt;

    // update bytes-count
#ifndef NO_BYTES_COUNTER
    lolliq->qlen_bytes += pkt->data_len;
#endif

    // update tail
    lolliq->tail = RING_IDX ( lolliq->tail + 1 );
    return 1;
}

static unsigned lolliq_enqueue_burst ( lolliqueue_t* lolliq, struct rte_mbuf** pkts, uint32_t num_pkts )
{
    // calculate number of packets to insert

    unsigned free_space = lolliq_free_count ( lolliq );

    if ( unlikely ( free_space < num_pkts ) ) {
        if ( unlikely ( free_space == 0 ) )  {
            return 0;
        } else {
            num_pkts = free_space;
        }
    }

    // enqueue packets starting from tail

    uint32_t ring_size = lolliq->ring_mask + 1;
    uint32_t ring_idx =  lolliq->tail;


    unsigned pkt_num;
    if ( likely ( ring_idx + num_pkts < ring_size ) ) {
//         for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++, ring_idx++ ) {
//             lolliq->ring[ring_idx] = pkts[pkt_num];
//         }
      // gcc doesn't verctorize this, and the macro in rte_ring also ... :(
      // so do memcpy instead

        memcpy(&lolliq->ring[ring_idx], pkts, num_pkts * sizeof(void*));

    } else {
        for ( pkt_num = 0; ring_idx < ring_size; pkt_num++, ring_idx++ ) {
            lolliq->ring[ring_idx] = pkts[pkt_num];
        }
        for ( ring_idx = 0; pkt_num < num_pkts; pkt_num++,ring_idx++ ) {
            lolliq->ring[ring_idx] = pkts[pkt_num];
        }
    }

#ifndef NO_BYTES_COUNTER
    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
        lolliq->qlen_bytes += pkts[pkt_num]->data_len;
    }
#endif


    lolliq->tail = RING_IDX ( lolliq->tail + num_pkts );
    return num_pkts;

    // hope this works :)
}

unsigned int lolliq_peek_burst ( lolliqueue_t* lolliq, struct rte_mbuf*** pkts )
{
    // dequeue

    uint32_t num_entries = lolliq_count ( lolliq );

//     LOG_VAR ( num_entries, "%u" );
//     LOG_VAR ( lolliq->head, "%u" );
//     LOG_VAR ( lolliq->tail, "%u" );

    uint32_t num_pkts = MAX_BURST;

    if ( unlikely ( num_pkts > num_entries ) ) {
        if ( unlikely ( num_entries == 0 ) ) {
            return 0;
        }
        num_pkts = num_entries;
    }

    const uint32_t ring_size = lolliq->ring_mask + 1;
    uint32_t ring_idx = RING_IDX ( lolliq->head + 1 );

    if ( likely ( ring_idx + num_pkts < ring_size ) ) {
        *pkts = &lolliq->ring[ring_idx];
    } else {
//         LOG_LINE();

        // copy packets from head to start to the lollipop_stick
        // and return pointer to the lollipop_stick

        unsigned pkts_to_copy = ring_size - ring_idx;
        // assert(pkts_to_copy < lolliq->burst_size);

//         LOG_VAR ( pkts_to_copy, "%u" );
//         LOG_VAR ( ring_size, "%u" );
//         LOG_VAR ( ring_idx, "%u" );

        // if packets are not on lollipop_stick, copy them there
        if ( lolliq->stick_index == 0 ) {

//             LOG_LINE();
//             LOG_VAR ( lolliq->burst_size - pkts_to_copy, "%u" );

            unsigned pkt_num;
            for ( pkt_num = MAX_BURST - pkts_to_copy; ring_idx < ring_size; pkt_num++,ring_idx++ ) {
                lolliq->lollipop_stick[pkt_num] = lolliq->ring[ring_idx];
            }
            lolliq->stick_index = pkts_to_copy;
        } /*else {
            if ( lolliq->stick_index != pkts_to_copy ) {
                LOG_VAR ( lolliq->stick_index, "%u" );
                LOG_VAR ( pkts_to_copy, "%u" );
            }
        }*/



//         LOG_VAR ( lolliq->burst_size - lolliq->stick_index, "%u" );
        *pkts = &lolliq->lollipop_stick[MAX_BURST - lolliq->stick_index];
    }

    return num_pkts;
}

/// pkts are to update qlen_bytes; it seems easier to do so, then to traverse ring again
void lolliq_dequeue_peeked_burst ( lolliqueue_t* lolliq, struct rte_mbuf** pkts, unsigned int num_sent_pkts )
{
    // update head
    lolliq->head = RING_IDX ( lolliq->head + num_sent_pkts );

    // update stick if present
    if ( unlikely ( lolliq->stick_index != 0 ) ) {
        if ( lolliq->stick_index > num_sent_pkts ) {
            lolliq->stick_index -= num_sent_pkts;
        } else {
            lolliq->stick_index = 0;
        }
    }

    // update byte queue counter
#ifndef NO_BYTES_COUNTER
    // update byte counter from already dequeued burst
    unsigned pkt_num;
    for ( pkt_num = 0; pkt_num < num_sent_pkts; pkt_num++ ) {
        lolliq->qlen_bytes -= pkts[pkt_num]->data_len;
    }
#endif
}

struct rte_mbuf* lolliq_head ( lolliqueue_t* lolliq )
{
    uint32_t ring_idx = RING_IDX ( lolliq->head + 1 );

    if ( unlikely ( ring_idx == lolliq->tail ) ) {
        return 0;
    } else {
        return lolliq->ring[ring_idx];
    }
}

void lolliq_drop_head ( lolliqueue_t* lolliq )
{
    // assert head is not null (lolliq_head was called and returned a packet)
    lolliq->head = RING_IDX ( lolliq->head + 1 );

    // if there is a peeked version decrease it too
    if ( lolliq->stick_index > 0 ) {
        lolliq->stick_index--;
    }

    struct rte_mbuf* pkt = lolliq->ring[lolliq->head];
#ifndef NO_BYTES_COUNTER
    lolliq->qlen_bytes -= pkt->data_len;
#endif
    rte_pktmbuf_free ( pkt );
}

void lolliq_dequeue_head ( lolliqueue_t* lolliq )
{
    // assert head is not null (lolliq_head was called and returned a packet)
    lolliq->head = RING_IDX ( lolliq->head + 1 );

    // if there is a peeked version decrease it too
//     if ( lolliq->stick_index > 0 ) {
//         lolliq->stick_index--;
//     }

#ifndef NO_BYTES_COUNTER
    struct rte_mbuf* pkt = lolliq->ring[lolliq->head];
    lolliq->qlen_bytes -= pkt->data_len;
#endif
//     rte_pktmbuf_free ( pkt );
}



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
