#ifndef	__BTAILQ__
#define	__BTAILQ__

#include <sys/queue.h>
#include "../rte_include.h"
#include "qelem_types.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * linked list of packet bursts
 */

#define MAX_PKT_BURST 64

struct rte_mbuf;

typedef struct __btailq_entry
{
  TAILQ_ENTRY ( __btailq_entry ) tailq_entry;
  tsc_cycles_t timestamp;
  unsigned num_pkts;
  struct rte_mbuf* pkts[MAX_PKT_BURST];
} btailq_entry_t;

typedef struct __btailq
{
  struct rte_mempool* btailq_entry_pool;
  TAILQ_HEAD ( __btailq_head, __btailq_entry ) tailq_head;
  uint64_t bytes_enqueued;
  uint64_t packets_enqueued;
  unsigned last_burst_first_notsent_packet;
} btailq_t;


static
void btaiq_init ( btailq_t* btailq )
{
  static int cnt = 0;
#define BTQM_NAME "btailq_mempool%d"
  char name[sizeof(BTQM_NAME)];
  sprintf(name, BTQM_NAME, cnt++);
  
  
  TAILQ_INIT ( &btailq->tailq_head );
  btailq->btailq_entry_pool = rte_mempool_create ( name,
                              512, sizeof ( btailq_entry_t ), 0, 0,
                              NULL, NULL, NULL, NULL,
                              rte_socket_id(),
                              MEMPOOL_F_SP_PUT | MEMPOOL_F_SC_GET // TODO: evaluate this
                                                 );
}

static
unsigned btailq_enqueue_burst ( btailq_t* btailq, struct rte_mbuf** pkts, uint32_t num_pkts, tsc_cycles_t now )
{

  // possible opt -> if now - tailq_tail.timestamp < timer_allowed_error add packetes to tail

  btailq_entry_t* entry;
  if ( rte_mempool_get ( btailq->btailq_entry_pool, ( void** ) &entry ) != 0 )
  {
    return 0;
  }

  entry->timestamp = now;
  entry->num_pkts = num_pkts;

  unsigned pkt_num;
  for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ )
  {
    btailq->bytes_enqueued += pkts[pkt_num]->data_len;
    entry->pkts[pkt_num] = pkts[pkt_num];
  }

  btailq->packets_enqueued += num_pkts;

  TAILQ_INSERT_TAIL ( &btailq->tailq_head, entry, tailq_entry );

  return num_pkts;
}

static
btailq_entry_t* btailq_peek_burst ( btailq_t* btailq )
{
  if ( btailq->tailq_head.tqh_first == NULL )
    return NULL;

  btailq_entry_t* entry = btailq->tailq_head.tqh_first;
  unsigned pkt_num;
  for ( pkt_num = btailq->last_burst_first_notsent_packet; pkt_num < entry->num_pkts; pkt_num++ )
  {
    btailq->bytes_enqueued -= entry->pkts[pkt_num]->data_len;
  }

  return entry;
}

static inline unsigned btailq_burst_num_pkts ( btailq_t* btailq, btailq_entry_t* entry )
{
  return entry->num_pkts - btailq->last_burst_first_notsent_packet;
}

static inline struct rte_mbuf** btailq_burst_ptr ( btailq_t* btailq, btailq_entry_t* entry )
{
  return &entry->pkts[btailq->last_burst_first_notsent_packet];
}

static
void btailq_on_burst_dequeued ( btailq_t * btailq, unsigned nb_deq )
{
  btailq->last_burst_first_notsent_packet+=nb_deq;
  btailq_entry_t* entry = btailq->tailq_head.tqh_first;

  if ( btailq->last_burst_first_notsent_packet >= entry->num_pkts )
  {
    // remove entry from the queue
    TAILQ_REMOVE ( &btailq->tailq_head, entry, tailq_entry );
    rte_mempool_put ( btailq->btailq_entry_pool, entry );

    btailq->last_burst_first_notsent_packet = 0;
  }
  else
  {
    unsigned pkt_num;
    for ( pkt_num = btailq->last_burst_first_notsent_packet; pkt_num < entry->num_pkts; pkt_num++ )
    {
      btailq->bytes_enqueued += entry->pkts[pkt_num]->data_len;
    }
  }

}


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
