#ifndef	__QDT_TSQUEUE__
#define	__QDT_TSQUEUE__

#include "../../rte_include.h"
#include "../qelem_types.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @file
 *
 * circular ring buffers for packets
 */

// #define PKTS_PER_ENTRY 32

// typedef struct {
//     tsc_cycles_t timestamps[PKTS_PER_ENTRY];
//     struct rte_mbuf* pkts[PKTS_PER_ENTRY];
//     unsigned first_index;
//     unsigned last_index;
// } tsq_entry_t;

typedef struct {
    tsc_cycles_t timestamp;
//     struct rte_mbuf* pkt;
} tsq_metadata_t;

typedef struct {
    unsigned tail; // last empty location (this is fifo -> write to tail, read from head)
    unsigned head; // first filled location
    unsigned peek_head;

    unsigned qlen_bytes;
//     unsigned num_entries; // must be pow2!
    unsigned ring_mask;

    tsq_metadata_t* metas;
    struct rte_mbuf** entries;
} tsqueue_t;

static size_t tsq_entries_sizeof ( unsigned num_entries );
static void tsq_init ( tsqueue_t* tsq, unsigned queue_len );

static unsigned tsq_len ( tsqueue_t* tsq );
static unsigned tsq_count ( tsqueue_t* tsq );
static unsigned tsq_free_count ( tsqueue_t* tsq );

static struct rte_mbuf* tsq_head ( tsqueue_t* tsq, tsc_cycles_t* timestamp );
static void tsq_drop_head ( tsqueue_t* tsq );
static qres_t tsq_enqueue_burst ( tsqueue_t* tsq, struct rte_mbuf **pkts,  uint32_t num_pkts, tsc_cycles_t now );
static unsigned tsq_peek_burst ( tsqueue_t* tsq, struct rte_mbuf** pkts, unsigned max_num_pkts );
static unsigned tsq_peek_burst1 ( tsqueue_t* tsq, struct rte_mbuf*** pkts, unsigned max_num_pkts );
static void tsq_dequeue_peeked ( tsqueue_t* tsq, struct rte_mbuf** pkts, unsigned first_unsent, unsigned num_pkts );

//---------------------------------------------------------------------------------------------------------------------------

#define RING_IDX(index) ((index) & tsq->ring_mask)
// #define ADJ(index) ((index) % (tsq->ring_mask + 1))

size_t tsq_entries_sizeof ( unsigned int num_entries )
{
    return num_entries * ( sizeof ( tsq_metadata_t ) + sizeof ( struct rte_mbuf* ) );
}

void tsq_init ( tsqueue_t* tsq, unsigned queue_len )
{
    // assume that tsq is zmalloced

    tsq->tail = 0;
    tsq->head = queue_len - 1;
//     tsq->num_entries = queue_len;
    tsq->ring_mask = queue_len - 1;

    tsq->metas = ( void* ) &tsq[1];
    tsq->entries = ( void* ) &tsq->metas[queue_len];

//     LOG_VAR ( queue_len, "%u" );

};

static unsigned tsq_enqueue_burst ( tsqueue_t* tsq, struct rte_mbuf **pkts,  uint32_t num_pkts, tsc_cycles_t now )
{

    // calculate number of packets to insert

    unsigned free_space = tsq_free_count ( tsq );

//     LOG_VAR ( free_space, "%u" );
//     LOG_VAR ( tsq->head, "%u" );
//     LOG_VAR ( tsq->tail, "%u" );

    unsigned num_to_insert;

    if ( free_space == 0 ) {
        return 0;
    } else if ( free_space < num_pkts ) {
        num_to_insert = free_space;
    } else {
        num_to_insert = num_pkts;
    }

    // insert packets starting from tail
    // currently, do the stupidest thing

    unsigned pkt_num, ring_idx = 0;
    for ( pkt_num = 0; pkt_num < num_to_insert; pkt_num++ ) {
        ring_idx = RING_IDX ( tsq->tail + pkt_num );
        tsq->metas[ring_idx].timestamp = now;
        tsq->entries[ring_idx] = pkts[pkt_num];
        tsq->qlen_bytes += pkts[pkt_num]->data_len;
    }

    tsq->tail =  RING_IDX ( tsq->tail + num_to_insert );

    return num_to_insert;

    // hope this works :)
}

struct rte_mbuf* tsq_head ( tsqueue_t* tsq, tsc_cycles_t* timestamp )
{
    if ( tsq_count ( tsq ) == 0 ) {
        return NULL;
    }

    unsigned head_idx = RING_IDX ( tsq->head + 1 );

    *timestamp = tsq->metas[head_idx].timestamp;
    return tsq->entries[head_idx];
}

void tsq_drop_head ( tsqueue_t* tsq )
{
    unsigned head_idx = RING_IDX ( tsq->head + 1 );

    tsq->qlen_bytes -= tsq->entries[head_idx]->data_len;
    rte_pktmbuf_free ( tsq->entries[head_idx] );
    tsq->head = head_idx;
}

unsigned int tsq_peek_burst ( tsqueue_t* tsq, struct rte_mbuf** pkts, unsigned int max_num_pkts )
{
    // dequeue

    unsigned pkt_num,ring_idx;
    for ( pkt_num = 0; pkt_num  < max_num_pkts; pkt_num++ ) {
        ring_idx = RING_IDX ( tsq->head + 1 + pkt_num );

//          LOG_VAR(idx, "%u");
//          LOG_VAR(idx2, "%u");

        if ( ring_idx == tsq->tail ) {
            break;
        }

        pkts[pkt_num] = tsq->entries [ring_idx] ;
        tsq->qlen_bytes -= tsq->entries [ring_idx]->data_len;
    }

    tsq->peek_head = RING_IDX ( tsq->head + pkt_num );
    return pkt_num;
}

unsigned int tsq_peek_burst1 ( tsqueue_t* tsq, struct rte_mbuf*** pkts, unsigned int max_num_pkts )
{
    unsigned num_elements = tsq_count ( tsq );
    unsigned num_to_peek = max_num_pkts > num_elements ? num_elements : max_num_pkts;

    unsigned peek_first,peek_last;
    
    // now check for overlap, if we overlap just return 
    // value without overlap // TODO: signal to ask for packets again
    if( tsq->head == tsq->ring_mask){
      // head is at the last element: return
      peek_first = 0;
      peek_last = num_to_peek;
    } else if ( tsq->head + 1 + num_to_peek > tsq->ring_mask + 1) {
        // no overlapping, dequeue packets and return pointer
	num_to_peek = (tsq->ring_mask+1) - (tsq->head + 1);
	peek_first = tsq->head + 1;
	peek_last = tsq->ring_mask;
    } else {
      peek_first = tsq->head + 1;
      peek_last = tsq->head + 1 + num_to_peek;
    }

    unsigned pkt_idx;
    for ( pkt_idx = peek_first; pkt_idx  < peek_last; pkt_idx++ ) {
        tsq->qlen_bytes -= tsq->entries [pkt_idx]->data_len;
    }
    
    *pkts = &tsq->entries[peek_first];
//     LOG_VAR(pkts, "%p");
//     LOG_VAR(*pkts, "%p");

    tsq->peek_head = peek_last;
    return num_to_peek;
}


void tsq_dequeue_peeked ( tsqueue_t* tsq, struct rte_mbuf** pkts, unsigned int num_sent, unsigned int num_pkts )
{
    // update qlen / all packets are in pkts
    unsigned idx;

    for ( idx = num_sent; idx < num_pkts; idx++ ) {
        tsq->qlen_bytes += pkts[idx]->data_len;
    }

    tsq->head =  RING_IDX ( tsq->head +  num_sent );
}

unsigned int tsq_len ( tsqueue_t* tsq )
{
    return tsq->qlen_bytes;
};

unsigned int tsq_count ( tsqueue_t* tsq )
{
    unsigned filled = RING_IDX ( tsq->tail - ( tsq->head + 1 ) );
    return filled;
}


unsigned int tsq_free_count ( tsqueue_t* tsq )
{
    unsigned free_space = RING_IDX ( tsq->head - ( tsq->tail ) ) ;
    return free_space;
}




#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
