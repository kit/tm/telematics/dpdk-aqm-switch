#ifndef	__QDT_LINKEDQ__
#define	__QDT_LINKEDQ__

#include "../../rte_include.h"
#include "../qelem_types.h"
#include <sys/queue.h>

#ifdef IN_IDE_PARSER
#define MAX_BURST 32
#endif

#ifndef MAX_BURST
#error please define MAX_BURST
#endif

#ifndef LQ_PARAM
#define LQ_PARAM
#define LQ_SET(entry)
#endif

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */


typedef struct m_entry
{
    STAILQ_ENTRY ( m_entry ) listptr;
#ifdef LQ_ENTRY_DATA
    LQ_ENTRY_DATA data;
#endif
} m_entry_t;

STAILQ_HEAD ( mbuf_list, m_entry );


typedef struct
{
    /* final */ unsigned qlen_max;

    struct mbuf_list qhead;
    unsigned qlen_current;

#ifndef NO_BYTES_COUNTER
    uint32_t qlen_bytes;
#endif

    struct rte_mbuf* burst[2 * MAX_BURST+3];
    unsigned burst_first;
    unsigned burst_count;
} linkedq_t;

// static size_t linkedq_entries_sizeof ( unsigned num_entries);
static void linkedq_init ( linkedq_t* linkedq, unsigned queue_len );

static unsigned linkedq_qlen_bytes ( linkedq_t* linkedq );
static unsigned linkedq_count ( linkedq_t* linkedq );
static unsigned linkedq_is_full ( linkedq_t* linkedq );

static unsigned int linkedq_enqueue_pkt ( linkedq_t* linkedq, struct rte_mbuf* pkt LQ_PARAM);
static unsigned int linkedq_enqueue_burst ( linkedq_t* linkedq, struct rte_mbuf** pkts, uint32_t num_pkts LQ_PARAM );
static unsigned linkedq_peek_burst ( linkedq_t* linkedq, struct rte_mbuf*** pkts );
static void linkedq_dequeue_peeked_burst ( linkedq_t* linkedq, struct rte_mbuf** pkts, unsigned num_pkts );

static struct rte_mbuf* linkedq_head ( linkedq_t* linkedq );
static void linkedq_drop_head ( linkedq_t* linkedq );
static void linkedq_dequeue_head ( linkedq_t* linkedq );

//---------------------------------------------------------------------------------------------------------------------------
#define MENTRY2MBUF(META) &(( struct rte_mbuf* ) META ) [-1];

void linkedq_init ( linkedq_t* linkedq, unsigned int queue_len )
{
    STAILQ_INIT ( &linkedq->qhead );
    linkedq->qlen_max = queue_len;
    linkedq->burst_first = 0;
    linkedq->burst_count = 0;

    // todo burst
};

unsigned int linkedq_qlen_bytes ( linkedq_t* linkedq )
{
#ifndef NO_BYTES_COUNTER
    return linkedq->qlen_bytes;
#else
    return 0;
#endif
}

unsigned int linkedq_count ( linkedq_t* linkedq )
{
    return linkedq->qlen_current;
}

unsigned int linkedq_is_full ( linkedq_t* linkedq )
{
    return linkedq->qlen_current >= linkedq->qlen_max;
}

unsigned int linkedq_enqueue_pkt ( linkedq_t* linkedq, struct rte_mbuf* pkt LQ_PARAM )
{
    if ( linkedq_is_full ( linkedq ) )
    {
        return 0;
    }

    m_entry_t* m_entry = ( m_entry_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkt , 0 );
    LQ_SET(m_entry);
    STAILQ_INSERT_TAIL ( &linkedq->qhead, m_entry, listptr );
    linkedq->qlen_current++;

#ifndef NO_BYTES_COUNTER
    linkedq->qlen_bytes += pkt->data_len;
#endif

    return 1;
}


static unsigned linkedq_enqueue_burst ( linkedq_t* linkedq, struct rte_mbuf** pkts, uint32_t num_pkts LQ_PARAM)
{
    unsigned pkt_num;

    for ( pkt_num = 0 ; pkt_num < num_pkts && linkedq->qlen_current < linkedq->qlen_max; pkt_num++, linkedq->qlen_current++ )
    {
        m_entry_t* m_entry = ( m_entry_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
        LQ_SET(m_entry);
        STAILQ_INSERT_TAIL ( &linkedq->qhead, m_entry, listptr );
    }

    unsigned num_enqueued = pkt_num;

#ifndef NO_BYTES_COUNTER
    for ( pkt_num = 0; pkt_num < num_enqueued; pkt_num++ )
    {
        linkedq->qlen_bytes += pkts[pkt_num]->data_len;
    }
#endif

    return num_enqueued;
}

// try to prefetch?
#define PREFETCH_PACKETS 3

// static unsigned linkedq_enqueue_burst ( linkedq_t* linkedq, struct rte_mbuf** pkts, uint32_t num_pkts LQ_PARAM)
// {
//     unsigned pkt_num;
//
//     // how many pkts to enqueue
//     unsigned free_space = linkedq->qlen_max - linkedq->qlen_current;
//     num_pkts = num_pkts > free_space ? free_space : num_pkts;
//
// //     LOG_VAR(num_pkts, "%u");
//
//
//     if ( unlikely (num_pkts  < PREFETCH_PACKETS) ) {
//         for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
//             m_entry_t* m_entry = ( m_entry_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
//             LQ_SET(m_entry);
//             STAILQ_INSERT_TAIL ( &linkedq->qhead, m_entry, listptr );
//         }
//         return num_pkts;
//     }
//
//     for ( pkt_num = 0; pkt_num < PREFETCH_PACKETS; pkt_num++ ) {
// //         rte_prefetch0 ( pkts[pkt_num] );
//         rte_prefetch0 ( rte_pktmbuf_mtod ( pkts[pkt_num], void* ) );
//     }
//
//     for ( pkt_num = 0; pkt_num < num_pkts-PREFETCH_PACKETS; pkt_num++ ) {
//         // prefetch next packet and process current packet
// //         rte_prefetch0 ( pkts[pkt_num+PREFETCH_PACKETS] );
//         rte_prefetch0 ( rte_pktmbuf_mtod ( pkts[pkt_num+PREFETCH_PACKETS], void* ) );
//
//         m_entry_t* m_entry = ( m_entry_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
//         LQ_SET(m_entry);
//         STAILQ_INSERT_TAIL ( &linkedq->qhead, m_entry, listptr );
//     }
//
//
//     for ( pkt_num = num_pkts-PREFETCH_PACKETS ; pkt_num < num_pkts; pkt_num++)
//     {
//         m_entry_t* m_entry = ( m_entry_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
//         LQ_SET(m_entry);
//         STAILQ_INSERT_TAIL ( &linkedq->qhead, m_entry, listptr );
//     }
//
// //     unsigned num_enqueued = pkt_num;
//
// // #ifndef NO_BYTES_COUNTER
// //     for ( pkt_num = 0; pkt_num < num_enqueued; pkt_num++ )
// //     {
// //         linkedq->qlen_bytes += pkts[pkt_num]->data_len;
// //     }
// // #endif
//
//     linkedq->qlen_current += num_pkts;
//     return num_pkts;
// }

// origignal

// unsigned int linkedq_peek_burst ( linkedq_t* linkedq, struct rte_mbuf*** pkts )
// {
//     m_entry_t* entry_ptr;
// 
//     entry_ptr = STAILQ_FIRST ( &linkedq->qhead );
//     if ( !entry_ptr )
//     {
//         return 0;
//     }
// 
//     unsigned pkt_num;
//     for ( pkt_num = 0;
//             pkt_num < ( MAX_BURST+1 ) && entry_ptr != NULL;
//             pkt_num++, entry_ptr = STAILQ_NEXT ( entry_ptr, listptr ) )
//     {
//         linkedq->burst[pkt_num] = MENTRY2MBUF ( entry_ptr );
//     }
// 
//     *pkts = linkedq->burst;
// 
//     unsigned num_pkts = pkt_num < MAX_BURST ? ( pkt_num ) : MAX_BURST;
//     return num_pkts;
// }

// TRY TO OPTIMIZE 1

unsigned int linkedq_peek_burst ( linkedq_t* linkedq, struct rte_mbuf*** pkts )
{
    // use burst-count as an indication that there are packets
    if( linkedq->burst_count > 0) {
        *pkts = linkedq->burst;
        return linkedq->burst_count-1;
    }  
  
    m_entry_t* entry_ptr;

    entry_ptr = STAILQ_FIRST ( &linkedq->qhead );
    if ( !entry_ptr )
    {
        return 0;
    }

    unsigned pkt_num;
    for ( pkt_num = 0;
            pkt_num < ( MAX_BURST+1 ) && entry_ptr != NULL;
            pkt_num++, entry_ptr = STAILQ_NEXT ( entry_ptr, listptr ) )
    {
        linkedq->burst[pkt_num] = MENTRY2MBUF ( entry_ptr );
    }

    *pkts = linkedq->burst;
    linkedq->burst_count = pkt_num;

    unsigned num_pkts = pkt_num < MAX_BURST ? ( pkt_num ) : MAX_BURST;
    return num_pkts;
}

// TRY TO OPTIMIZE 2
// unsigned int linkedq_peek_burst ( linkedq_t* linkedq, struct rte_mbuf*** pkts )
// {
//     m_entry_t* entry_ptr;
//
//     // check whether there are pkts in the burst
//     if(linkedq->burst_count != 0) {
//         // if we have at least MAX_BURST/2 pkts just return them
//         if(linkedq->burst_count >= MAX_BURST / 2) {
// // 	    LOG_LINE();
//             *pkts = &linkedq->burst[linkedq->burst_first];
//             return linkedq->burst_count;
//         }
//
//         // if we have space for whole max burst - start with burst->first
//         // else reset
//         if(linkedq->burst_first + linkedq->burst_count > 2 * MAX_BURST +1) {
// // 	    LOG_LINE();
//             linkedq->burst_count = 0;
// 	    linkedq->burst_first = 0;
//         }
//     } else {
// // 	    LOG_LINE();
//         linkedq->burst_first = 0;
//     }
//
// //     LOG_VAR(linkedq->burst_first, "%u");
// //     LOG_VAR(linkedq->burst_count, "%u");
//
//
//     entry_ptr = STAILQ_FIRST ( &linkedq->qhead );
//     if ( !entry_ptr )
//     {
//         return 0;
//     }
//
//     unsigned first_pkt = linkedq->burst_first + linkedq->burst_count;
//
// //     LOG_VAR(first_pkt, "%u");
//
//     unsigned pkt_num;
//     for ( pkt_num = first_pkt;
//             pkt_num < first_pkt + ( MAX_BURST+1 ) && entry_ptr != NULL;
//             pkt_num++, entry_ptr = STAILQ_NEXT ( entry_ptr, listptr ) )
//     {
//         linkedq->burst[pkt_num] = MENTRY2MBUF ( entry_ptr );
//     }
//
// //     LOG_VAR(pkt_num - first_pkt, "%u");
//
//     *pkts = linkedq->burst + first_pkt;
//     linkedq->burst_count += pkt_num - first_pkt;
//
//     unsigned num_pkts = linkedq->burst_count < MAX_BURST ? ( linkedq->burst_count ) : MAX_BURST;
//     return num_pkts;
// }

/// pkts are to update qlen_bytes; it seems easier to do so, then to traverse ring again
void linkedq_dequeue_peeked_burst ( linkedq_t* linkedq, struct rte_mbuf** pkts, unsigned int num_sent_pkts )
{

    linkedq->qlen_current -= num_sent_pkts;

    // check if there are still packets in the queue
    if(linkedq->qlen_current > 0) {
        STAILQ_FIRST ( &linkedq->qhead ) = ( m_entry_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[num_sent_pkts], 0 );
    } else {
        // if not reset queue
        STAILQ_INIT ( &linkedq->qhead );
    }

//     linkedq->burst_first += num_sent_pkts;
    linkedq->burst_count = 0;

    // update byte queue counter
#ifndef NO_BYTES_COUNTER
    // update byte counter from already dequeued burst
    unsigned pkt_num;
    for ( pkt_num = 0; pkt_num < num_sent_pkts; pkt_num++ )
    {
        linkedq->qlen_bytes -= pkts[pkt_num]->data_len;
    }
#endif
}

struct rte_mbuf* linkedq_head ( linkedq_t* linkedq )
{
    m_entry_t* entry_ptr = STAILQ_FIRST ( &linkedq->qhead );
    if(!entry_ptr) 
      return 0;
    
    struct rte_mbuf* pkt = MENTRY2MBUF ( entry_ptr );
    return pkt;
}

void linkedq_drop_head ( linkedq_t* linkedq )
{
    m_entry_t* entry_ptr = STAILQ_FIRST ( &linkedq->qhead );
    struct rte_mbuf* pkt = MENTRY2MBUF ( entry_ptr );

#ifndef NO_BYTES_COUNTER
    linkedq->qlen_bytes -= rte_pktmbuf_data_len ( pkt );
#endif

    STAILQ_REMOVE_HEAD(&linkedq->qhead, listptr);    
    rte_pktmbuf_free ( pkt );
    linkedq->qlen_current --;
}

void linkedq_dequeue_head ( linkedq_t* linkedq )
{

#ifndef NO_BYTES_COUNTER
    m_entry_t* entry_ptr = STAILQ_FIRST ( &linkedq->qhead );
    struct rte_mbuf* pkt = MENTRY2MBUF ( entry_ptr );
    linkedq->qlen_bytes -= rte_pktmbuf_data_len ( pkt );
#endif

    STAILQ_REMOVE_HEAD(&linkedq->qhead, listptr);  
    linkedq->qlen_current --;
}



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
