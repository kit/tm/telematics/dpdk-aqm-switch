#include "qelem.h"
#include "qelem_fq_codel.h"
#include "qelem_types.h"
#include "qtrl/codel.h"
#include <sys/queue.h>
#include "../rte_include.h"
#include "../config/log_macros.h"
#include "../datapath/packet_metadata.h"

#define BURST_SIZE 32
#define MAX_TAILQ_NUMBER 4

static qelem_ops_t qelem_fq_codel_ops;

//-------------------------------------------------------------------------------------------------------------------------

TAILQ_HEAD ( subq_pkt_head, __fq_codel_metadata );

typedef struct __subqueue
{
  // entry for linked list of new/old queues
  TAILQ_ENTRY ( __subqueue ) queues_list_entry;

  // head of the pkt queue
  struct subq_pkt_head subq_pkt_head;

  // queue state variables
  boolean_t is_in_list_of_new_queues;
  boolean_t is_in_list_of_old_queues;
  unsigned bytes_enq;
  int deficit;

  codel_state_t perq_codel_state;

} subqueue_t;

TAILQ_HEAD ( queues_list, __subqueue );

typedef struct
{
  qelem_t qelem;

  codel_params_t global_codel_params;
  qls_t* classifier;

  struct queues_list new_queues;
  struct queues_list old_queues;

  unsigned pkt_limit;
  unsigned curr_pkt_counter;

  unsigned subq_num;
  subqueue_t subqueues[0];

} qelem_fq_codel_t ;

#define QUANTUM 1514


typedef struct __fq_codel_metadata
{
  uint64_t timestamp;
  TAILQ_ENTRY ( __fq_codel_metadata ) pkt_queue_entry;
  struct rte_mbuf* parent_pkt;

} fq_codel_metadata_t;

//-------------------------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_fq_codel_alloc (const  qelem_params_t* common_params,const  void* qelem_params )
{
  LOG_INIT ( "allocating qelem_fq_codel_lowspeed for port %d \n", common_params->port_id );

  const qelem_fq_codel_params_t* params = (const  qelem_fq_codel_params_t* ) qelem_params;
  qelem_fq_codel_t* qelem = rte_zmalloc ( "qelem_fq_codel", sizeof ( qelem_fq_codel_t ) + params->num_subqueues * sizeof ( subqueue_t ), RTE_CACHE_LINE_SIZE );

  qelem->qelem.v_table = &qelem_fq_codel_ops;
  qelem->qelem.port_id = common_params->port_id;
  qelem->subq_num = params->num_subqueues;

  qelem->classifier = params->qls_create ( params->qls_params );

  TAILQ_INIT ( &qelem->new_queues );
  TAILQ_INIT ( &qelem->old_queues );

//   subqueue_t** subqueues_ptrs = qelem->subqueues;
  unsigned qnum;
  for ( qnum = 0; qnum < qelem->subq_num; qnum++ )
  {
    TAILQ_INIT ( & qelem->subqueues[qnum].subq_pkt_head );
    ctrl_codel_init ( &qelem->subqueues[qnum].perq_codel_state, &qelem->global_codel_params,
                      params->interval_sec, params->target_sec, params->mtu, rte_get_tsc_hz() );
  }

  qelem->pkt_limit = params->pkt_hard_limit;
  return ( qelem_t* ) qelem;
};

static
void qelem_fq_codel_free ( qelem_t* qelem )
{
  qelem_fq_codel_t* qelem0 = ( qelem_fq_codel_t* ) qelem;
  rte_free ( qelem0->subqueues );
  rte_free ( qelem0 );
};

//-------------------------------------------------------------------------------------------------------------------------

static
unsigned qelem_fq_codel_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts , unsigned hw_queue_index )
{
  qelem_fq_codel_t* fq_codel = ( qelem_fq_codel_t* ) qelem;

  unsigned  pkt_num;
  tsc_cycles_t now = rte_get_tsc_cycles();

  for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ )
  {
    // read hash value from packet metadata
    uint32_t flow_key_sig = qls_qlassify ( fq_codel->classifier, pkts[pkt_num] );
//     LOG_VAR ( flow_key_sig, "0x%.8x" );
    uint32_t subq_num = flow_key_sig % fq_codel->subq_num;
    subqueue_t* subq = &fq_codel->subqueues[subq_num];

    // from this point we don't need packet metadata, replace it
    fq_codel_metadata_t* fq_codel_entry = ( fq_codel_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
    fq_codel_entry->timestamp = now;
    fq_codel_entry->parent_pkt = pkts[pkt_num];


    TAILQ_INSERT_TAIL ( &subq->subq_pkt_head, fq_codel_entry, pkt_queue_entry );
    subq->bytes_enq += pkts[pkt_num]->data_len;

    if ( !subq->is_in_list_of_new_queues && !subq->is_in_list_of_old_queues )
    {
      // if subq is in neither of the lists -> add it to the new subq list
      subq->deficit = QUANTUM;
      subq->is_in_list_of_new_queues = TRUE;
      TAILQ_INSERT_TAIL ( &fq_codel->new_queues, subq, queues_list_entry );
    }
    else if ( subq->is_in_list_of_new_queues )
    {
      // if subq was in the list of new queues, remove it from the list
      // of new queues and add to the list of old queues
      subq->is_in_list_of_new_queues = FALSE;
      subq->is_in_list_of_old_queues = TRUE;
      TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
      TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
    }
    else
    {
      // subq is in the list of old queues -> reinsert it at the tail
      TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
      TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
    }

    fq_codel->curr_pkt_counter++;


    if ( fq_codel->curr_pkt_counter >= fq_codel->pkt_limit )
    {
      subqueue_t* iterq,*victimq = 0;
      unsigned max = 0;
      TAILQ_FOREACH ( iterq, &fq_codel->new_queues, queues_list_entry )
      {
        if ( iterq->bytes_enq > max )
        {
          victimq = iterq;
          max = iterq->bytes_enq;
        }
      }
      TAILQ_FOREACH ( iterq, &fq_codel->old_queues, queues_list_entry )
      {
        if ( iterq->bytes_enq > max )
        {
          victimq = iterq;
          max = iterq->bytes_enq;
        }
      }

      // drop packet from the queue
// this can't happen if we have 10240 packets in the queue if ( victimq && !TAILQ_EMPTY(&victimq->subq_pkt_head)) {
      fq_codel_metadata_t* pktm = TAILQ_FIRST ( &victimq->subq_pkt_head );
      TAILQ_REMOVE ( &victimq->subq_pkt_head , pktm, pkt_queue_entry );
      victimq->bytes_enq-=pktm->parent_pkt->data_len;
      rte_pktmbuf_free ( pktm->parent_pkt );
//             }
    }
  }

  return pkt_num;
};

static subqueue_t* fq_codel_select_subq ( qelem_fq_codel_t* fq_codel, boolean_t* is_new_queue )
{
  subqueue_t* subq = 0;

  // traverse list of new queues
  TAILQ_FOREACH ( subq, &fq_codel->new_queues, queues_list_entry )
  {
    if ( subq->deficit < 0 )
    {
      subq->deficit += QUANTUM;
      subq->is_in_list_of_new_queues = FALSE;
      subq->is_in_list_of_old_queues = TRUE;
      TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
      TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
    }
    else
    {
      *is_new_queue=TRUE;
      return subq;
    }
  }

  // traverse list of old queues
  TAILQ_FOREACH ( subq, &fq_codel->old_queues, queues_list_entry )
  {
    if ( subq->deficit < 0 )
    {
      subq->deficit += QUANTUM;
      TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
      TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
    }
    else
    {
      *is_new_queue=FALSE;
      return subq;
    }
  }

  return 0;
}



static void fq_codel_do_codel_drops ( qelem_fq_codel_t* fq_codel, subqueue_t* subq , tsc_cycles_t now )
{
  FQ_CODEL_TRACE_SUBQ(subq, subq->bytes_enq, subq->deficit);
  
  // return wether we need to send packet or not
  while ( 1 )
  {
    if ( TAILQ_EMPTY ( & subq->subq_pkt_head ) )
    {
      ctrl_codel_on_pkt_dequeue ( &subq->perq_codel_state, &fq_codel->global_codel_params, 0, now, 0 );
      return;
    }

    // dequeue packet from the queue
    fq_codel_metadata_t* pktm = TAILQ_FIRST ( &subq->subq_pkt_head );

    // consult codel on whether to drop the packet
    boolean_t should_drop = ctrl_codel_on_pkt_dequeue ( &subq->perq_codel_state, &fq_codel->global_codel_params,
                            pktm->timestamp, now , subq->bytes_enq );

    if ( should_drop )
    {
      TAILQ_REMOVE ( &subq->subq_pkt_head , pktm, pkt_queue_entry );
      subq->bytes_enq -= pktm->parent_pkt->data_len;
      fq_codel->curr_pkt_counter--;
      rte_pktmbuf_free ( pktm->parent_pkt );
      continue;
    }
    else
    {
      return;
    }
  }
}

// static struct rte_mbuf* fq_codel_dequeue_one ( qelem_fq_codel_t* fq_codel )
// {
//   while ( 1 )
//   {
//     boolean_t is_new_queue = 0;
//     subqueue_t* subq = fq_codel_select_subq ( fq_codel, &is_new_queue );
//     if ( !subq )
//     {
//       return 0;
//     }
//
//     struct rte_mbuf* pkt = fq_codel_run_codel_on_queue ( fq_codel, subq );
//     if ( pkt )
//     {
//       subq->deficit -= pkt->data_len;
//       return pkt;
//     }
//
//     // if queue is empty
//     if ( is_new_queue )
//     {
//       subq->is_in_list_of_new_queues = FALSE;
//       subq->is_in_list_of_old_queues = TRUE;
//       TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
//       TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
//     }
//     else
//     {
//       // remove queue from the list of active queues
//       subq->is_in_list_of_new_queues = FALSE;
//       subq->is_in_list_of_old_queues = FALSE;
//       TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
//     }
//   }
// }

static
void qelem_fq_codel_xmit ( qelem_t *_qelem, unsigned hw_queue_index )
{
  qelem_fq_codel_t* fq_codel = ( qelem_fq_codel_t* ) _qelem;
  tsc_cycles_t now = rte_get_tsc_cycles();

  while ( 1 )
  {
    boolean_t is_new_queue = 0;
    subqueue_t* subq = fq_codel_select_subq ( fq_codel, &is_new_queue );
    if ( !subq )
    {
      return;
    }

    // drop packets according to codel's law
    fq_codel_do_codel_drops ( fq_codel, subq , now );

    if ( !TAILQ_EMPTY ( &subq->subq_pkt_head ) )
    {
      // try to send packet, if failed, leave pkt where it is
      fq_codel_metadata_t* pktm = TAILQ_FIRST ( &subq->subq_pkt_head );
      struct rte_mbuf* pkt = pktm->parent_pkt;
      // we will remove packet because if the packet is sent we don't own the mbuf anymore
      TAILQ_REMOVE ( &subq->subq_pkt_head , pktm, pkt_queue_entry );
      unsigned pkt_len = pkt->data_len;
      boolean_t sent = rte_eth_tx_burst ( fq_codel->qelem.port_id, hw_queue_index, &pkt, 1 ) == 1;

      if ( sent )
      {
        subq->deficit -= pkt_len;
        fq_codel->curr_pkt_counter--;
      }
      else
      {
        // if the packet was not sent, put it back in queue
        TAILQ_INSERT_HEAD ( &subq->subq_pkt_head, pktm, pkt_queue_entry );
      }
      return;
    }
    else
    {
      // if queue is empty
      if ( is_new_queue )
      {
        subq->is_in_list_of_new_queues = FALSE;
        subq->is_in_list_of_old_queues = TRUE;
        TAILQ_REMOVE ( &fq_codel->new_queues, subq, queues_list_entry );
        TAILQ_INSERT_TAIL ( &fq_codel->old_queues, subq, queues_list_entry );
      }
      else
      {
        // remove queue from the list of active queues
        subq->is_in_list_of_new_queues = FALSE;
        subq->is_in_list_of_old_queues = FALSE;
        TAILQ_REMOVE ( &fq_codel->old_queues, subq, queues_list_entry );
      }
    }
  }

//   struct rte_mbuf* pkt = fq_codel_dequeue_one ( fq_codel );
//   if ( !pkt )
//   {
//     return;
//   }
//
//   while (rte_eth_tx_burst ( fq_codel->qelem.port_id, hw_queue_index, &pkt, 1 ) != 1);
}

qelem_factory_ops_t qelem_fq_codel_factory =
{
  .qelem_alloc = qelem_fq_codel_alloc,
  .qelem_free = qelem_fq_codel_free,
};

static qelem_ops_t qelem_fq_codel_ops =
{
  .enqueue_burst = qelem_fq_codel_enqueue_burst,
  .xmit = qelem_fq_codel_xmit,
};

