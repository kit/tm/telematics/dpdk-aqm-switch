#include "rte_port_qos_ethdev_writer.h"
#include "qlog.h"

typedef struct {
    struct rte_mbuf* burst[2 * RTE_PORT_IN_BURST_SIZE_MAX];
    uint32_t tx_burst_size;
    uint32_t tx_burst_count;
    qelem_t *qelem;
    unsigned hw_queue_index;
} port_qos_ethdev_writer_t;

static void *
port_qos_ethdev_writer_create ( void *params, int socket_id )
{
    struct rte_port_qos_ethdev_writer_params *conf = ( struct rte_port_qos_ethdev_writer_params * ) params;
    // TODO: check parameters

    port_qos_ethdev_writer_t *qos_port;

    qos_port = rte_zmalloc_socket ( "PORT", sizeof ( port_qos_ethdev_writer_t ), RTE_CACHE_LINE_SIZE, socket_id );
    if ( qos_port == NULL ) {
        RTE_LOG ( ERR, PORT, "%s: Failed to allocate port\n", __func__ );
        return NULL;
    }


    qos_port->tx_burst_size = conf->tx_burst_size;
    qos_port->qelem = conf->qelem;
    qos_port->hw_queue_index = conf->hw_queue_index;

    return qos_port;
}

static inline void
xmit ( port_qos_ethdev_writer_t *qos_port )
{
  
//     LOG_LINE();
//     LOG_VAR(qos_port->qelem->v_table, "%p");
//     LOG_VAR(qos_port->qelem->v_table->enqueue_burst, "%p");
//     LOG_VAR(qos_port->qelem->v_table->xmit, "%p");
    // TODO: when to call this method -> after enqueueing or after writing someting to port
    // TODO: main concern - latency
    qelem_xmit ( qos_port->qelem, qos_port->hw_queue_index );
}

static inline void
enqueue_burst ( port_qos_ethdev_writer_t *qos_port )
{
    unsigned nb_enc;
    nb_enc = qelem_enqueue_burst ( qos_port->qelem, qos_port->burst, qos_port->tx_burst_count/*, qos_port->hw_queue_index */);
    qos_port->tx_burst_count = 0;

    xmit ( qos_port );
}


static int
port_qos_ethdev_writer_tx ( void *port, struct rte_mbuf *pkt )
{
    port_qos_ethdev_writer_t *qos_port = ( port_qos_ethdev_writer_t * ) port;

    qos_port->burst[qos_port->tx_burst_count++] = pkt;
    if ( qos_port->tx_burst_count >= qos_port->tx_burst_size ) {
        enqueue_burst ( qos_port );
    }

    return 0;
}

static int
port_qos_ethdev_writer_tx_bulk ( void *port, struct rte_mbuf **pkts, uint64_t pkts_mask )
{
    port_qos_ethdev_writer_t *qos_port = ( port_qos_ethdev_writer_t * ) port;

    // add pkt burst to the burst already pending
    // and if we reached min burst size - send burst

    if ( ( pkts_mask & ( pkts_mask+1 ) ) == 0 ) {

        uint64_t n_pkts = __builtin_popcountll ( pkts_mask );

        unsigned pkt_num;
        for ( pkt_num = 0; pkt_num < n_pkts; pkt_num++ ) {
            qos_port->burst[qos_port->tx_burst_count++] = pkts[pkt_num];
        }

    } else {
        // gather packets and enqueue them

        for ( ; pkts_mask; ) {
            uint32_t pkt_index = __builtin_ctzll ( pkts_mask );
            uint64_t pkt_mask = 1LLU << pkt_index;
            struct rte_mbuf *pkt = pkts[pkt_index];

            qos_port->burst[qos_port->tx_burst_count++] = pkt;
            pkts_mask &= ~pkt_mask;
        }

    }

    if ( qos_port->tx_burst_count >= qos_port->tx_burst_size ) {
        enqueue_burst ( qos_port );
    }

    return 0;
}

static int
port_qos_ethdev_writer_flush ( void *port )
{
    enqueue_burst ( ( port_qos_ethdev_writer_t * ) port );
    return 0;
}

static int
port_qos_ethdev_writer_free ( void *port )
{
    if ( !port ) {
        return -EINVAL;
    }

    port_qos_ethdev_writer_flush ( port );
    rte_free ( port );

    return 0;
}

struct rte_port_out_ops rte_port_qos_ethdev_writer_ops = {
    .f_create = port_qos_ethdev_writer_create,
    .f_free = port_qos_ethdev_writer_free,
    .f_tx = port_qos_ethdev_writer_tx,
    .f_tx_bulk = port_qos_ethdev_writer_tx_bulk,
    .f_flush = port_qos_ethdev_writer_flush,
};


