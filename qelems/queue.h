#ifndef	__QELEM_QUEUE__
#define	__QELEM_QUEUE__

#include "../rte_include.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @FILE
 *
 * datastructure for queues (as in AQM)
 *
 * enqueue behaves like rte_ring producers
 * consumer's (tx) side:
 * 	consumer is guarded by spinlock/trylock - if someone is sending packets there is no need for the second person to send packets
 * 	consumer takes a bulk of packets, calls rte_tx_burst, then updates the head by the number of successfully transmitted packets
 * 		which is essentially peek
 */


typedef struct __queue {

    struct __enc_side {
        volatile uint32_t enc_head;
        volatile uint32_t enc_tail;

    } enc_side __rte_cache_aligned;

    struct __tx_side {
        volatile uint32_t tx_head;
        volatile uint32_t tx_tail;
    } tx_side __rte_cache_aligned;

    unsigned size;
    struct rte_mbuf* pkts[0] __rte_cache_aligned;

} queue_t;


queue_t* queue_alloc ( unsigned size );
int queue_sp_enqueue_burst ( queue_t* queue, struct rte_mbuf** pkts, unsigned num_pkts );
// queue_st_tx(queue_t* queue, struct rte_mbuf** pkts, unsigned num_pkts);


inline
queue_t* queue_alloc ( unsigned size )
{
    // TODO: check that count is 2^n

    queue_t* queue = rte_malloc ( 0, sizeof ( queue_t ) + sizeof ( void* ) *size, 64 );
    if ( !queue ) {
        rte_panic ( "couldn't alloc queue" );
    }
    memset(queue, 0, sizeof(queue_t));
    

    queue->size = size;

    return queue;
}

inline
int queue_sp_enqueue_burst ( queue_t* queue, struct rte_mbuf** pkts, unsigned int num_pkts )
{
    // step one  - copy enc_head and tx_tail into local variables
    // and check whether there is enough space
    uint32_t enc_head, tx_tail;
    
    enc_head = queue->enc_side.enc_head;
    tx_tail = queue->tx_side.tx_tail;
    
    if(tx_tail - enc_head < num_pkts) {
      num_pkts = tx_tail - enc_head;
    }
    
    // step two - update enc_head to reserve space for the elements
    // and copy packets
    unsigned pkt_num;
    queue->enc_side.enc_head += num_pkts;
    enc_head += num_pkts;
    struct rte_mbuf** qpkts = &queue->pkts[queue->enc_side.enc_tail];
    for(pkt_num = 0; pkt_num < num_pkts; pkt_num++){
      qpkts[pkt_num] = pkts[pkt_num];
    }
    
    // step three - update enc_tail
    queue->enc_side.enc_tail =  queue->enc_side.enc_head;
    
    return num_pkts;
}




#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
