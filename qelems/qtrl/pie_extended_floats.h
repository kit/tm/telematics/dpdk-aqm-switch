#ifndef __QTRL_PIE__
#define __QTRL_PIE__

#include "../qelem_types.h"
#include "../traces/pie_event.h"
#include "../qmath/fastrand.h"

#ifdef IN_IDE_PARSER
#define PIE_USE_SOJOURN
#define PIE_USE_EXTENDED
#endif


#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @FILE
 *
 * pie controller - PIE extended version from draft-ieft-aqm-pie-02 with queue drain rate estimation
 */

typedef struct
{
  // configurable parameters
  double REFERENCE_DELAY; // keep it in doule to avoid problems with negative unsigned integers
  tsc_cycles_t MAX_BURST_TIME;

  // possibly configurable parameters
  // for periodic updates
  tsc_cycles_t t_update_tsc;
  double alpha,beta;
  // for depature rate estimation
  unsigned qlen_thresh; // min queue len to estimate
  double epsilon;

  unsigned mtu;

} pie_params_t;

typedef struct
{
  // variables for drop probability calc.
  double p;
  double accu_p;
  tsc_cycles_t burst_allow_time;

  // variables for periodic updates
  tsc_cycles_t last_update;
  double avg_drain_rate;
#ifdef PIE_USE_SOJOURN
  // for sojourn-based pie
  tsc_cycles_t curr_qdelay;
  double old_qdelay; // keep it in doule to avoid problems with negative unsigned integers
#else
  double old_delay;
#endif

  // variables for depature rate estimation
  int drained_bytes;
  tsc_cycles_t last_measurement;

  rand_state_t rand_state;

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif
} pie_state_t;

//---------------------------------------------------------------------------------------------------
static inline void
ctrl_pie_init ( pie_state_t* state, pie_params_t* params,  tsc_cycles_t timer_freq,
                seconds_t ref_delay, seconds_t t_update, double alpha, double beta,
                void* trace_queue );

static inline boolean_t
ctrl_pie_on_pkt_enqueue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned int qlen_bytes );

#ifdef PIE_USE_SOJOURN
static inline void
ctrl_pie_on_pkt_dequeue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t last_sojourn );
#else
static inline void
ctrl_pie_on_pkt_dequeue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes, unsigned pkt_size );
#endif


static inline void
ctrl_pie_update_if_needed ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes );

//---------------------------------------------------------------------------------------------------
#define DRAINED_BYTES_INVALID -1
#define PIE_IN_MEASUREMENT(pie) (pie->drained_bytes != DRAINED_BYTES_INVALID)

static inline double
__pie_rand ( pie_state_t* state )
{
  uint32_t rand = fast_rand ( &state->rand_state );
  return ( ( double ) rand ) / ( ( double ) ~0u );
}

static inline boolean_t
__pie_should_drop ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned int qlen_bytes )
{
// TODO: recheck all this for drain-rate version

  // reset burst_allow (Sec 4.4)
  if ( ( state->p == 0 ) && ( state->curr_qdelay < params->REFERENCE_DELAY / 2 )
       && ( state->old_qdelay < params->REFERENCE_DELAY / 2 ) )
  {
    if ( state->burst_allow_time != params->MAX_BURST_TIME ) // this if is for logging
    {
      state->burst_allow_time = params->MAX_BURST_TIME;
      pie_on_ballow_reset(state->trace_queue, now , params->MAX_BURST_TIME);
    }
  }

  // allow bursts for some time
  if ( state->burst_allow_time > 0 )
  {
    return FALSE;
  }

  //[draft pseudocode] safeguard PIE to be work conserving (Sec.4.1)
  if ( ( state->old_qdelay < params->REFERENCE_DELAY / 2 ) && ( state->p < 0.2 ) )
  {
    return FALSE;
  }
  // second part of the sefeguard
  if ( qlen_bytes < 2 * params->mtu )
  {
    return FALSE;
  }

  // safeguard from random dropping (Sec 5.4)
  if ( state->p == 0 )
  {
    state->accu_p = 0;
  }
  state->accu_p += state->p;

  boolean_t should_drop = FALSE;

  if ( state->accu_p < 0.85 )
  {
    should_drop = FALSE;
  }
  else if ( state->accu_p >= 8.5 )
  {
    should_drop = TRUE;
  }
  else
  {
    double rand = __pie_rand ( state );
    should_drop = rand <= state->p;
  }

  if ( should_drop )
  {
    state->accu_p = 0;
  }

  return should_drop;
}

static void
__pie_update_drop_probability ( pie_state_t* state, const pie_params_t* params, unsigned qlen_bytes, tsc_cycles_t now )
{

  // TODO get curr_delay
#ifdef PIE_USE_SOJOURN
  double curr_delay = (double)state->curr_qdelay;
#else
  double curr_delay;
  if ( state->avg_drain_rate != 0 )
  {
    curr_delay = ( ( double ) qlen_bytes ) / state->avg_drain_rate;
  }
  else
  {
    curr_delay = 0;
  }
#endif

// so we somehow got current delay

  double alpha = params->alpha;
  double beta = params->beta;
  double old_p = state->p;

  // TODO:
  // assume that p table in the draft is in percents
  // because of values in Bob Briscoe's review
  double delta_p = alpha * ( curr_delay - params->REFERENCE_DELAY ) + beta * ( curr_delay - state->old_qdelay );
//   LOG_VAR(delta_p, "%f");
  
  if ( old_p < 0.000001 ) // 0.0001%
  {
    delta_p /= 2048;
  }
  else if ( old_p < 0.00001 ) // 0.001%
  {
    delta_p /= 512;
  }
  else if ( old_p < 0.0001 ) // 0.01%
  {
    delta_p /= 128;
  }
  else if ( old_p < 0.001 ) // 0.1%
  {
    delta_p /= 32;
  }
  else if ( old_p < 0.01 ) // 1%
  {
    delta_p /= 8;
  }
  else if ( old_p < 0.1 ) // 10%
  {
    delta_p /= 2;
  }

  // cap drop adjustment delta_p (Sec 5.5)
  if ( state->p >= 0.1 && delta_p > 0.02 )
  {
    delta_p = 0.02;
  }

  state->p += delta_p;

  // exponentially decay drop prob when congestion goes away (Sec.4.2)
  if ( curr_delay == 0 &&  state->old_qdelay == 0 )
  {
    state->p *= 0.98;    //1- 1/64 is sufficient
  }

  // make sure p is between 0 and 1
  if ( state->p < 0 )
  {
    state->p = 0;
  }
  if ( state->p > 1 )
  {
    state->p = 1;
  }

  // update burst allow (Sec 4.4)
  if ( state->burst_allow_time > params->t_update_tsc )
  {
    state->burst_allow_time -= params->t_update_tsc;
  }
  else
  {
    state->burst_allow_time = 0;
  }
  
 
  state->old_qdelay = curr_delay;
  pie_on_update ( state->trace_queue, now, curr_delay, state->p, state->burst_allow_time );
}

static void
__pie_calculate_depature_rate ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes, unsigned pkt_size )
{
  // if queue is big enough and measurement is turned off -> turn measurement on
  if ( qlen_bytes > params->qlen_thresh * 1514   && !PIE_IN_MEASUREMENT ( state ) )
  {
    state->drained_bytes = 0;
//     state->last_measurement = now;
    state->last_measurement = rte_rdtsc(); // TODO check - doing more preceise dq
    pie_on_enter_measurement ( state->trace_queue, now, state->burst_allow_time );
  }

  if ( PIE_IN_MEASUREMENT ( state ) )
  {
    state->drained_bytes += pkt_size;

    if ( state->drained_bytes >= ( int ) params->qlen_thresh * 1514 )
    {
//       tsc_cycles_t drain_interval = now - state->last_measurement;
      tsc_cycles_t drain_interval = rte_rdtsc() - state->last_measurement; // TODO check - doing more preceise dq
      double drain_rate = ( ( double ) state->drained_bytes ) / ( double ) drain_interval;

      if ( state->avg_drain_rate == 0 )
      {
        state->avg_drain_rate = drain_rate;
      }
      else
      {
        state->avg_drain_rate = ( 1 - params->epsilon ) * state->avg_drain_rate + params->epsilon * drain_rate;
      }

      // if qlen is too small stop measuring
      // else reset measurement cycle
      if ( qlen_bytes < params->qlen_thresh )
      {
        state->drained_bytes = DRAINED_BYTES_INVALID;
        pie_on_exit_measurement ( state->trace_queue, now, state->burst_allow_time );
      }
      else
      {
        state->last_measurement = now;
        state->drained_bytes = 0;
      }

      pie_on_new_drain_rate_estimation ( state->trace_queue, now, drain_rate, state->avg_drain_rate, state->burst_allow_time );
    }
  }
}

//---------------------------------------------------------------------------------------------------

void ctrl_pie_init ( pie_state_t* state, pie_params_t* params,  tsc_cycles_t timer_freq,
                     seconds_t ref_delay, seconds_t t_update, double alpha, double beta,
                     void* trace_queue )
{
  // TODO:
  params->REFERENCE_DELAY = ref_delay  * timer_freq;
  params->MAX_BURST_TIME = 75e-3  * timer_freq; // this is hard-coded in linux too
  LOG_PARAM ( params->REFERENCE_DELAY ,"%f" );

  params->t_update_tsc = t_update * timer_freq;

  params->alpha = alpha / timer_freq;
  params->beta = beta / timer_freq;

//   params->qlen_thresh = 10; // it should be about 10 packets, or something else in byte mode
  params->qlen_thresh = 64; // it should be about 10 packets, or something else in byte mode
  params->epsilon = 0.125; // this is hard-coded
//   params->epsilon = 0.250;

  params->mtu = 1514; // this we can hardcode ...

  state->drained_bytes = DRAINED_BYTES_INVALID;
  state->avg_drain_rate = 0;
  state->burst_allow_time = params->MAX_BURST_TIME;
  state->p = 0;
  fast_srand ( &state->rand_state, rte_rdtsc() );
//   state->rand_state.g_seed = rte_rdtsc();

#ifdef WITH_TRACES
  state->trace_queue = trace_queue;
#endif
}

void ctrl_pie_update_if_needed ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned int qlen_bytes )
{
  if ( now - state->last_update > params->t_update_tsc )
  {
    __pie_update_drop_probability ( state, params, qlen_bytes,now );
    state->last_update = now;
  }
}

boolean_t ctrl_pie_on_pkt_enqueue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned int qlen_bytes )
{
  return __pie_should_drop ( state, params, now, qlen_bytes );
}

#ifdef PIE_USE_SOJOURN
void ctrl_pie_on_pkt_dequeue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t last_sojourn )
{
  state->curr_qdelay = last_sojourn;
}
#else
void ctrl_pie_on_pkt_dequeue ( pie_state_t* state, const pie_params_t* params, tsc_cycles_t now, unsigned qlen_bytes, unsigned pkt_size )
{
  __pie_calculate_depature_rate ( state,params, now, qlen_bytes, pkt_size );
}
#endif

static inline
void ctrl_pie_on_taildrop ( pie_state_t* state, const pie_params_t* params )
{
  state->accu_p = 0;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
