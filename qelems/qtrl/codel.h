#ifndef	__QTRL_CODEL__
#define	__QTRL_CODEL__

// TODO bob briscoes pow -3/4 for cubic

// parameters:
// 	-D APPROX_MATH use approximate sqrt root
#define APPROX_MATH
//	-D FLAVOR  = DRAFT (reset counter-= 2) linux (see linux) cake (counter /= 2)
#define FLAVOR_DRAFT 0
#define FLAVOR_LINUX 1
#define FLAVOR FLAVOR_LINUX

#include "../qelem_types.h"
#ifdef APPROX_MATH
#include "../qmath/approx_reciprocal_sqrt.h"
#endif
#include "../traces/codel_event.h"

#ifdef FQ_CODEL
#define FQ_QUEUE_VAR_DECL , void* fq_queue_ptr
#define FQ_QUEUE_PARAM ,fq_queue_ptr
#else
#define FQ_QUEUE_VAR_DECL
#define FQ_QUEUE_PARAM
#endif

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

/**
 * @FILE
 *
 * CODEL AQM Logic
 */

typedef struct {
    tsc_cycles_t interval;
    tsc_cycles_t target;
    unsigned mtu;
} codel_params_t;

typedef struct {
    // estimator
    tsc_cycles_t persisten_queue_since;

    // controller
    boolean_t is_in_dropping_state;
    tsc_cycles_t drop_next;
    uint32_t pkts_dropped;
#if FLAVOR==FLAVOR_LINUX
    uint32_t pkts_dropped_last;
#endif
#ifdef APPROX_MATH
    Q_0_32 approx_inv_sqrt;
#endif

#ifdef WITH_TRACES
    event_log_queue_t* trace_queue;
#endif

} codel_state_t;

//---------------------------------------------------------------------------------------------------
// interface aka public methods
//---------------------------------------------------------------------------------------------------

static inline void ctrl_codel_init ( codel_state_t* codel_state, codel_params_t* codel_params,
                                     double interval_s, double target_s, unsigned mtu_bytes, tsc_cycles_t timer_freq,
                                     boolean_t init_params, void* trace_queue
                                   );

static inline boolean_t ctrl_codel_on_pkt_dequeue ( codel_state_t* codel_state, const codel_params_t* codel_params,
        tsc_cycles_t pkt_enq_time, tsc_cycles_t now, unsigned qlen_bytes FQ_QUEUE_VAR_DECL
                                                  );
//----------------------------------------------------------------------------------------------------------------

static inline
boolean_t __codel_congestion_estimator ( codel_state_t* state, const codel_params_t* params, tsc_cycles_t enqueued_time, tsc_cycles_t now, unsigned qlen_bytes )
{
    tsc_cycles_t sojourn_time = enqueued_time ? now - enqueued_time : 0;

    // check if sojourn_time is above the target
    if ( sojourn_time > params->target && qlen_bytes > 1 * params->mtu ) {
        // check if sojourn_time was above the target for at least an interval
        if ( state->persisten_queue_since == 0 ) {
            // it there was no persistent queue... indicate that there will be
            state->persisten_queue_since = now + params->interval;
        } else {
            // if there was persistent queue ...
            if ( now >= state->persisten_queue_since ) {
                return TRUE;
            }
        }
    } else {
        // min sojourn_time is not above the target
        state->persisten_queue_since = 0;
    }

    return FALSE;
}

#ifdef APPROX_MATH
static inline
uint64_t __codel_control_law ( codel_state_t* state, const codel_params_t* params )
{
    return state->drop_next + int_q_0_32_mul ( params->interval, state->approx_inv_sqrt ); // interval / sqrt ( pkts_dropped );
}
#else
#include <math.h>
static inline
uint64_t __codel_control_law ( codel_state_t* state, const codel_params_t* params )
{
    return state->drop_next + ( ( double ) params->interval ) / sqrt ( state->pkts_dropped );
}
#endif

static inline
void __codel_controller_exit_congestion_state ( codel_state_t* state, tsc_cycles_t now FQ_QUEUE_VAR_DECL )
{
    if ( state->is_in_dropping_state ) {
        state->is_in_dropping_state = FALSE;
        codel_on_exit_dropping_state ( state->trace_queue, now FQ_QUEUE_PARAM );
    }
}

static inline
void __codel_controller_enter_congestion_state ( codel_state_t* state, const codel_params_t* params, tsc_cycles_t now FQ_QUEUE_VAR_DECL)
{
    // this function should only be called if the queue is in persistent queue state

    // if we were not in dropping state, enter dropping state
    // also make opt. when dropping state is entered/exited too often
    if ( !state->is_in_dropping_state ) {
        state->is_in_dropping_state = TRUE;
        codel_on_enter_dropping_state ( state->trace_queue, now FQ_QUEUE_PARAM);

#if FLAVOR == FLAVOR_DRAFT
        state->pkts_dropped = ( state->pkts_dropped >= 2 ) && ( ( now - state->drop_next ) < 8 * params->interval )
                              ? state->pkts_dropped - 2 : 0;
#elif FLAVOR == FLAVOR_LINUX
        uint32_t delta = state->pkts_dropped - state->pkts_dropped_last;
        if ( delta > 1 && ( ( now - state->drop_next )  < 16 * params->interval ) ) {
            state->pkts_dropped = delta-1;
        } else {
            state->pkts_dropped = 0;
        }
        state->pkts_dropped_last = state->pkts_dropped;
#else
#error ("no codel flavor defined ... please define flavor");
#endif
        state->drop_next = now-1;
    }
}

static inline
boolean_t __codel_controller_should_drop_pkt ( codel_state_t* state, const codel_params_t* params, tsc_cycles_t now FQ_QUEUE_VAR_DECL)
{
    // this function should only be called if the queue is in persistent queue state
    __codel_controller_enter_congestion_state ( state,  params, now FQ_QUEUE_PARAM);

    // was there a packet that was dropped in the current dropping interval?
    if ( now >= state->drop_next ) {
        state->pkts_dropped++;
#ifdef APPROX_MATH
        approx_rec_sqrt_next ( &state->approx_inv_sqrt , state->pkts_dropped );
#else
	// handle overflow before 1/sqrt
        if(state->pkts_dropped == 0) state->pkts_dropped = 1;
#endif
        state->drop_next = __codel_control_law ( state, params );
        codel_on_aqm_drop ( state->trace_queue, now,  state->drop_next, state->pkts_dropped FQ_QUEUE_PARAM);
        return TRUE;
    } else {
        return FALSE;
    }
};

//-------------------------------------------------------------------------------------------------------------

/** initializes codel */
void ctrl_codel_init ( codel_state_t* codel_state, codel_params_t* codel_params,
                       double interval_sec, double target_sec, unsigned int mtu_bytes,
                       tsc_cycles_t timer_freq,  boolean_t init_params, void* trace_queue )
{
    if ( init_params ) {
        double interval_tsc = ( double ) interval_sec  * timer_freq;
        double target_tsc = ( double ) target_sec * timer_freq;

        codel_params->interval = interval_tsc;
        codel_params->target  = target_tsc;
        codel_params->mtu = mtu_bytes;

        LOG_PARAM ( timer_freq, "%lu" );
        LOG_PARAM ( interval_tsc, "%f" );
        LOG_PARAM ( target_tsc, "%f" );
        LOG_PARAM ( codel_params->interval, "%lu" );
        LOG_PARAM ( codel_params->target, "%lu" );
    }

#ifdef APPROX_MATH
    approx_rec_sqrt_reset ( &codel_state->approx_inv_sqrt );
#endif

#ifdef WITH_TRACES
    codel_state->trace_queue = trace_queue;
#endif
//   CODEL_LOG_HEADER();

}

/** return whether the packet should be dropped */
boolean_t ctrl_codel_on_pkt_dequeue ( codel_state_t* codel_state, const codel_params_t* codel_params,
                                      tsc_cycles_t pkt_enq_time, tsc_cycles_t now, unsigned qlen_bytes FQ_QUEUE_VAR_DECL )
{
    // call estimator
    boolean_t is_in_congestion = __codel_congestion_estimator ( codel_state, codel_params, pkt_enq_time, now, qlen_bytes );

    if ( !is_in_congestion ) {
        __codel_controller_exit_congestion_state ( codel_state , now FQ_QUEUE_PARAM);
        return FALSE;
    }

    // in congestion -> call controller
    return __codel_controller_should_drop_pkt ( codel_state, codel_params, now FQ_QUEUE_PARAM);
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

