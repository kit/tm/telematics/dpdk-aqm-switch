#ifndef	__QTRL_GSP__
#define	__QTRL_GSP__

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */


#ifdef IN_IDE_PARSER
#define DELAY_GSP
#endif

#include "../qelem_types.h"
#include "../traces/gsp_event.h"

#ifdef DELAY_GSP
#define ADAPT_PARAM_TYPE tsc_cycles_t
#define ADAPT_PARAM_NAME pkt_sojourn
#else
#define ADAPT_PARAM_TYPE unsigned
#define ADAPT_PARAM_NAME qlen_bytes_before
#endif

typedef enum  {CLEAR, OVERFLOW, DRAIN} qstate_t;

typedef struct
{
  // basic algorithm
#ifdef DELAY_GSP
  tsc_cycles_t threshold_tsc;
#else
  unsigned int threshold;
#endif

  // adaptation params
  unsigned alpha;
  long unsigned tau;
  tsc_cycles_t initial_interval;

  // max threshold for cummulative_time
  tsc_cycles_t max_cummulative_time;
} gsp_params_t;

typedef struct
{
  // basic algorithm
  tsc_cycles_t timeout_expiry;

  // adaptation
  tsc_cycles_t interval;

  qstate_t astate;
  tsc_cycles_t cummulative_time;
  tsc_cycles_t last_timestamp;

#ifdef DELAY_GSP
  tsc_cycles_t last_sojourn;
#endif

#ifdef WITH_TRACES
  event_log_queue_t* trace_queue;
#endif

} gsp_state_t;

//----------------------------------------------------------------------------------------------------------------

static inline
void __gsp_adaptation ( gsp_state_t* state, const gsp_params_t* params, tsc_cycles_t now, ADAPT_PARAM_TYPE ADAPT_PARAM_NAME )
{

  tsc_cycles_t delta_t = now - state->last_timestamp;
  state->last_timestamp = now;

  // accumulate time
#ifdef DELAY_GSP
  if ( pkt_sojourn > params->threshold_tsc )
#else
  if ( qlen_bytes_before > params->threshold )
#endif
  {
    state->cummulative_time += delta_t * params->alpha ;
  }
  else
  {
    if ( likely ( state->astate == CLEAR ) )
    {
      if ( state->cummulative_time > delta_t )
      {
        state->cummulative_time -= delta_t;
      }
      else
      {
        state->cummulative_time = 0;
      }
    }
  }

  if ( state->cummulative_time > params->max_cummulative_time )
  {
    state->cummulative_time = params->max_cummulative_time;
  };

  // update interval
  state->interval = ( params->initial_interval * params->tau ) / ( params->tau + state->cummulative_time );
//   gsp_on_adaptation ( state->trace_queue, now, state->cummulative_time, state->interval, state->astate );

#ifdef DELAY_GSP
  // save sojourn
  state->last_sojourn = pkt_sojourn;
#endif
}

//----------------------------------------------------------------------------------------------------------------

static inline void ctrl_gsp_init ( gsp_state_t* state, gsp_params_t* params, const qelem_gsp_params_t* qparams,
                                   tsc_cycles_t timer_freq, tsc_cycles_t now, void* trace_queue )
{

#ifdef DELAY_GSP
  double threshold_tsc = qparams->threshold_ratio  * timer_freq;
  params->threshold_tsc = threshold_tsc;
#else
  params->threshold =  qparams->qlen_bytes * qparams->threshold_ratio ;
#endif
  params->initial_interval = qparams->interval_s  * timer_freq;

  params->alpha = ( qparams->alpha > 0 ) ? qparams->alpha > 0 : 2;
  params->tau = ( qparams->tau > 0 ) ? qparams->tau > 0 : 5;
  // tau is N * interval
  params->tau = params->tau * params->initial_interval;

  params->max_cummulative_time = timer_freq * 2 * 60; // 2 mins //TODO: something like expiry > time to send 10 packets

  // assume state is zmalloced
  state->astate = CLEAR;
  state->last_timestamp = now;
  state->timeout_expiry = now; // should be set on enqueue

  LOG_PARAM ( qparams->qlen_bytes, "%u" );
#ifdef DELAY_GSP
  LOG_PARAM ( params->threshold_tsc, "%lu" );
#else
  LOG_PARAM ( params->threshold, "%u" );
#endif
  LOG_PARAM ( params->initial_interval, "%lu" );


#ifdef WITH_TRACES
  state->trace_queue = trace_queue;
#endif

};

static inline void ctrl_gsp_on_pkts_dequeue (
  gsp_state_t* state, const gsp_params_t* params,
  tsc_cycles_t now, ADAPT_PARAM_TYPE ADAPT_PARAM_NAME )
{
  __gsp_adaptation ( state, params, now, ADAPT_PARAM_NAME );
}


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
