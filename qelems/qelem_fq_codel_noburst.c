#include "qelems.h"
#include "../rte_include.h"
// #include "../config/log_macros.h"
#include "qlog.h"
#include "macros/common.h"
#include "qls.h"
#include <sys/queue.h>
#define FQ_CODEL
#include "qtrl/codel.h"

//-------------------------------------------------------------------------------------------------------------------------

#define MBUF(FQ_PKT) &(( struct rte_mbuf* ) FQ_PKT ) [-1];

static qelem_ops_t qelem_fq_codel_ops;

enum LIST {NONE = 0, NEW_QUEUES, OLD_QUEUES};

TAILQ_HEAD ( flow_queue_head, __fq_codel_metadata );

typedef struct __flow_queue {
    // entry for linked list of new/old queues
    TAILQ_ENTRY ( __flow_queue ) queues_list_entry;

    // head of the pkt queue
    struct flow_queue_head fqueue_head;
    enum LIST list;

    // queue state variables
    unsigned bytes_enq;
    int deficit;

    codel_state_t perq_codel_state;

} flow_queue_t;

TAILQ_HEAD ( queues_list, __flow_queue );

typedef struct {
    qelem_t qelem;

    // parameters
    codel_params_t global_codel_params;
    qls_t* classifier;
    unsigned quantum;
    unsigned pkt_limit;

    // states
    struct queues_list new_queues;
    struct queues_list old_queues;
    unsigned curr_pkt_counter;

    // for token bucket
    flow_queue_t* peeked_queue;
    struct rte_mbuf* peeked_pkt[1];
    uint64_t qlen_bytes;

    // data structure
    unsigned num_flow_queues;
    flow_queue_t flow_queues[0];
} qelem_fq_codel_t ;

// #define QUANTUM 1514


typedef struct __fq_codel_metadata {
    uint64_t timestamp;
    TAILQ_ENTRY ( __fq_codel_metadata ) flow_queue_entry;
//     struct rte_mbuf* parent_pkt; // TODO replace with container_of

} fq_codel_metadata_t;

//-------------------------------------------------------------------------------------------------------------------------

static
qelem_t* qelem_fq_codel_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    LOG_INIT ( "allocating qelem_fq_codel_lowspeed for port %d \n", common_params->port_id );

    QPARAMSCAST ( fq_codel );
    qelem_fq_codel_t* qelem = 0;

    if ( common_params->min_send_burst != 1 ) {
        rte_panic ( "fq-codel can currenlty only work with bursts of 1 packet ..." );
    }

    QZMALLOC ( qelem, qelem_fq_codel_t, sizeof ( qelem_fq_codel_t ) + params->num_flow_queues * sizeof ( flow_queue_t ) );
    qelem_init ( ( qelem_t* ) qelem, common_params, &qelem_fq_codel_ops );

    qelem->num_flow_queues = params->num_flow_queues;
    qelem->classifier = params->qls_create ( params->qls_params );
    qelem->quantum = params->quantum;
    qelem->pkt_limit = params->pkt_hard_limit;

    TAILQ_INIT ( &qelem->new_queues );
    TAILQ_INIT ( &qelem->old_queues );

    unsigned qnum;
    for ( qnum = 0; qnum < qelem->num_flow_queues; qnum++ ) {
        TAILQ_INIT ( & qelem->flow_queues[qnum].fqueue_head );
        ctrl_codel_init ( &qelem->flow_queues[qnum].perq_codel_state, &qelem->global_codel_params,
                          params->interval_sec, params->target_sec, params->mtu,  rte_get_tsc_hz(), qnum == 0,
                          common_params->trace_queue );
    }

    return ( qelem_t* ) qelem;
};

//-----------------------------------------------------------------------------------------------------------
// helper opts
//-----------------------------------------------------------------------------------------------------------

static inline
void fq_codel_dequeue_head ( qelem_fq_codel_t* fq_codel, flow_queue_t* flow_queue, boolean_t was_pkt_sent, tsc_cycles_t now )
{
    fq_codel_metadata_t* fq_pkt = TAILQ_FIRST ( &flow_queue->fqueue_head );
    struct rte_mbuf* pkt = MBUF ( fq_pkt );

    TAILQ_REMOVE ( &flow_queue->fqueue_head , fq_pkt, flow_queue_entry );
    flow_queue->bytes_enq-=pkt->data_len;

    fq_codel->curr_pkt_counter --;
    fq_codel->qlen_bytes-=pkt->data_len;

    if ( was_pkt_sent ) {
        flow_queue->deficit -= pkt->data_len;
#if defined WITH_TRACES && WITH_TRACES == 2
        unsigned sojourn_time = now - fq_pkt->timestamp;
        codel_on_pkt_dequeue ( flow_queue->perq_codel_state.trace_queue, now, sojourn_time, flow_queue->bytes_enq, flow_queue );
#endif 
    } else {
        rte_pktmbuf_free ( pkt );

    }
}

static inline
void fq_codel_enqueue_burst ( qelem_fq_codel_t* fq_codel, struct rte_mbuf **pkts,  uint32_t num_pkts, tsc_cycles_t now )
{
    unsigned pkt_num;
    for ( pkt_num = 0; pkt_num < num_pkts; pkt_num++ ) {
        // read hash value from packet metadata
        uint32_t flow_key_sig = qls_qlassify ( fq_codel->classifier, pkts[pkt_num] );
        uint32_t flow_queue_num = flow_key_sig % fq_codel->num_flow_queues;
        flow_queue_t* flow_queue = &fq_codel->flow_queues[flow_queue_num];

        // from this point we don't need packet metadata, replace it
        fq_codel_metadata_t* fq_codel_entry = ( fq_codel_metadata_t* ) RTE_MBUF_METADATA_UINT8_PTR ( pkts[pkt_num], 0 );
        fq_codel_entry->timestamp = now;

        TAILQ_INSERT_TAIL ( &flow_queue->fqueue_head, fq_codel_entry, flow_queue_entry );
        flow_queue->bytes_enq += pkts[pkt_num]->data_len;
        fq_codel->qlen_bytes += pkts[pkt_num]->data_len;

        switch ( flow_queue->list ) {
        case NONE:
            // if flow is in neither of the lists -> add it to the new subq list
            flow_queue->deficit = fq_codel->quantum;
            flow_queue->list = NEW_QUEUES;
            TAILQ_INSERT_TAIL ( &fq_codel->new_queues, flow_queue, queues_list_entry );
            break;
        case NEW_QUEUES:
            // if flow_queue was in the list of new queues, remove it from the list
            // of new queues and add to the list of old queues
            flow_queue->list = OLD_QUEUES;
            TAILQ_REMOVE ( &fq_codel->new_queues, flow_queue, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, flow_queue, queues_list_entry );
            break;
        case OLD_QUEUES:
            // flow queue is in the list of old queues -> reinsert it at the tail
            TAILQ_REMOVE ( &fq_codel->old_queues, flow_queue, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, flow_queue, queues_list_entry );
            break;
        }

        fq_codel->curr_pkt_counter++;

        if ( fq_codel->curr_pkt_counter >= fq_codel->pkt_limit ) {
            flow_queue_t* iterq,*victimq = 0;
            unsigned max = 0;
            TAILQ_FOREACH ( iterq, &fq_codel->new_queues, queues_list_entry ) {
                if ( iterq->bytes_enq > max ) {
                    victimq = iterq;
                    max = iterq->bytes_enq;
                }
            }
            TAILQ_FOREACH ( iterq, &fq_codel->old_queues, queues_list_entry ) {
                if ( iterq->bytes_enq > max ) {
                    victimq = iterq;
                    max = iterq->bytes_enq;
                }
            }

//             LOG_LINE();

            // drop packet from the queue (victimq can't be empty if there are 10240 packets in the queue)
            fq_codel_on_pkt_overflow ( flow_queue->perq_codel_state.trace_queue, now, flow_queue );
            fq_codel_dequeue_head ( fq_codel, victimq, FALSE, now );
        }
    }
    
    fq_codel_on_pkt_enqueue(fq_codel->flow_queues[0].perq_codel_state.trace_queue, now, fq_codel->curr_pkt_counter);
}

static inline
void fq_codel_run_codel_on_fqueue ( qelem_fq_codel_t* fq_codel, flow_queue_t* flow_queue , tsc_cycles_t now )
{
    // return wether we need to send packet or not
    while ( 1 ) {
        if ( TAILQ_EMPTY ( & flow_queue->fqueue_head ) ) {
            ctrl_codel_on_pkt_dequeue ( &flow_queue->perq_codel_state, &fq_codel->global_codel_params, 0, now, 0, flow_queue );
            return;
        }

        // peek packet from the queue
        fq_codel_metadata_t* pkt_metadata = TAILQ_FIRST ( &flow_queue->fqueue_head );

        // consult codel on whether to drop the packet
        boolean_t should_drop = ctrl_codel_on_pkt_dequeue ( &flow_queue->perq_codel_state, &fq_codel->global_codel_params,
                                pkt_metadata->timestamp, now , flow_queue->bytes_enq, flow_queue );

        if ( should_drop ) {
            fq_codel_dequeue_head ( fq_codel, flow_queue, FALSE , now );
            continue;
        } else {
            return;
        }
    }
}

static inline
flow_queue_t* fq_codel_select_flow_queue ( qelem_fq_codel_t* fq_codel )
{
    flow_queue_t* flow_queue = 0;

    // traverse list of new queues
    TAILQ_FOREACH ( flow_queue, &fq_codel->new_queues, queues_list_entry ) {
        if ( flow_queue->deficit < 0 ) {
            flow_queue->deficit += fq_codel->quantum;
            flow_queue->list = OLD_QUEUES;
            TAILQ_REMOVE ( &fq_codel->new_queues, flow_queue, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, flow_queue, queues_list_entry );
        } else {
            return flow_queue;
        }
    }

    // traverse list of old queues
    TAILQ_FOREACH ( flow_queue, &fq_codel->old_queues, queues_list_entry ) {
        if ( flow_queue->deficit < 0 ) {
            flow_queue->deficit += fq_codel->quantum;
            TAILQ_REMOVE ( &fq_codel->old_queues, flow_queue, queues_list_entry );
            TAILQ_INSERT_TAIL ( &fq_codel->old_queues, flow_queue, queues_list_entry );
        } else {
            return flow_queue;
        }
    }

    return 0;
}

static inline
flow_queue_t* fq_codel_select_flow_queue2 ( qelem_fq_codel_t* fq_codel, tsc_cycles_t now )
{
    while ( 1 ) {

        flow_queue_t* flow_queue = fq_codel_select_flow_queue ( fq_codel );
        if ( !flow_queue ) {
            break;
        }

        // run codel AQM on the queue
        fq_codel_run_codel_on_fqueue ( fq_codel, flow_queue , now );

        // if the queue becomes empty -> process according to fq-algorithm
        if ( TAILQ_EMPTY ( &flow_queue->fqueue_head ) ) {
            if ( flow_queue->list == NEW_QUEUES ) {
                flow_queue->list = OLD_QUEUES;
                TAILQ_REMOVE ( &fq_codel->new_queues, flow_queue, queues_list_entry );
                TAILQ_INSERT_TAIL ( &fq_codel->old_queues, flow_queue, queues_list_entry );
            } else {
                // remove queue from the list of active queues
                flow_queue->list = NONE;
                TAILQ_REMOVE ( &fq_codel->old_queues, flow_queue, queues_list_entry );
            }
        } else {
            return flow_queue;
        }
    }

    return NULL;
}

static inline
void fq_codel_tx_pkt ( qelem_fq_codel_t* fq_codel, tsc_cycles_t now )
{
    flow_queue_t* flow_queue = fq_codel_select_flow_queue2 ( fq_codel, now );
    if ( !flow_queue ) {
        return;
    }

    fq_codel_metadata_t* pkt_metadata = TAILQ_FIRST ( &flow_queue->fqueue_head );
    struct rte_mbuf* pkt =MBUF ( pkt_metadata );

    boolean_t sent = rte_eth_tx_burst ( fq_codel->qelem.port_id, fq_codel->qelem.queue_id, &pkt, 1 ) == 1;
    if ( sent ) {
        //WARNING: same assumptions that were for the design of Lolliq hold here
        fq_codel_dequeue_head ( fq_codel, flow_queue, TRUE , now );
    }
}

//-----------------------------------------------------------------------------------------------------------
// default opts
//-----------------------------------------------------------------------------------------------------------

static
void qelem_fq_codel_tx_flush ( qelem_t *qelem )
{
    QCAST ( fq_codel );

    if ( fq_codel->curr_pkt_counter > 0 ) {
        tsc_cycles_t now = rte_get_tsc_cycles();
        fq_codel_tx_pkt ( fq_codel, now );
    }
}

static
void qelem_fq_codel_tx_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( fq_codel );
    tsc_cycles_t now = rte_get_tsc_cycles();

    fq_codel_enqueue_burst ( fq_codel, pkts, num_pkts, now );
    fq_codel_tx_pkt ( fq_codel, now );
}

static
void qelem_fq_codel_free ( qelem_t* qelem )
{
    qelem_fq_codel_t* qelem0 = ( qelem_fq_codel_t* ) qelem;
    rte_free ( qelem0->flow_queues );
    rte_free ( qelem0 );
};

//---------------------------------------------------------------------------------------------------------------
// optional opts
//---------------------------------------------------------------------------------------------------------------

static
unsigned qelem_fq_codel_enqueue_burst ( qelem_t *qelem, struct rte_mbuf **pkts,  uint32_t num_pkts )
{
    QCAST ( fq_codel );
    tsc_cycles_t now = rte_get_tsc_cycles();
    fq_codel_enqueue_burst ( fq_codel, pkts, num_pkts, now );
    return num_pkts;
};

static
unsigned qelem_fq_codel_peek_burst ( qelem_t *qelem, struct rte_mbuf*** burst_ptr )
{
    QCAST ( fq_codel );
    tsc_cycles_t now = rte_get_timer_cycles();

    flow_queue_t* flow_queue = fq_codel_select_flow_queue2 ( fq_codel, now );
    if ( !flow_queue ) {
        return 0;
    }

    fq_codel->peeked_queue = flow_queue;
    fq_codel_metadata_t* fq_pkt = TAILQ_FIRST ( &flow_queue->fqueue_head );
    struct rte_mbuf* pkt = MBUF ( fq_pkt );
    fq_codel->peeked_pkt[0] = pkt;

    *burst_ptr = fq_codel->peeked_pkt;
    return 1;
}

static
void qelem_fq_codel_dequeue_peeked_burst ( qelem_t *qelem, struct rte_mbuf** burst_ptr, unsigned num_pkts )
{
    QCAST ( fq_codel );

    flow_queue_t* flow_queue =   fq_codel->peeked_queue;
    struct rte_mbuf* pkt = burst_ptr[0];

#if defined WITH_TRACES && WITH_TRACES == 2
    tsc_cycles_t now = rte_get_timer_cycles();
#else
    tsc_cycles_t now = 0;
#endif

    //WARNING: same assumptions that were for the design of Lolliq hold here
    fq_codel_dequeue_head ( fq_codel, flow_queue, TRUE , now);
}


static
unsigned qelem_fq_codel_queue_len_bytes ( qelem_t * qelem )
{
    QCAST ( fq_codel );
    return fq_codel->qlen_bytes;
}

static
unsigned qelem_fq_codel_queue_len_pkts ( qelem_t * qelem )
{
    QCAST ( fq_codel );
    return fq_codel->curr_pkt_counter;
}

//-----------------------------------------------------------------------------------------------------------
// trace log
//-----------------------------------------------------------------------------------------------------------

#ifdef WITH_TRACES
static
void* fq_codel_trace_queue_alloc ( const  qelem_params_t* common_params,const  void* qelem_params )
{
    return trace_event_queue_add ( "fq_codel", common_params->trace_queue_size, sizeof ( fq_codel_event_t ), fq_codel_event_log );
}
#endif

//------------------------------------------------------------------------------------------------------------

qelem_factory_t qelem_fq_codel_factory = {
    .qelem_alloc = qelem_fq_codel_alloc,
#ifdef WITH_TRACES
    .trace_queue_alloc = fq_codel_trace_queue_alloc,
#endif
};

static qelem_ops_t qelem_fq_codel_ops = {
    .qelem_tx_burst = qelem_fq_codel_tx_burst,
    .qelem_tx_flush = qelem_fq_codel_tx_flush,
    .qelem_free = qelem_fq_codel_free,

    .enqueue_burst = qelem_fq_codel_enqueue_burst,
    .peek_burst = qelem_fq_codel_peek_burst,
    .dequeue_peeked_burst = qelem_fq_codel_dequeue_peeked_burst,
    .queue_len_bytes = qelem_fq_codel_queue_len_bytes,
    .queue_len_pkts = qelem_fq_codel_queue_len_pkts,
};




