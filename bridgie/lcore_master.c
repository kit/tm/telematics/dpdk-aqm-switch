// #include "../src/lcores.h"
#include "../bridgie/bridgie.h"
#include "../rte_include.h"
#include "../bridgie/log_macros.h"
// #include "../datapath/switch_stats.h"
#include "../qelems/qdev.h"
#include "../qelems/traces/trace_event_queue.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static
void switch_stats_print1 ( switch_stats_t* old_stats, switch_stats_t* curr_stats, uint64_t old_tsc, uint64_t curr_tsc );
void qelem_stats_print1 ( qelem_stats_t* old_stats, qelem_stats_t* curr_stats, uint64_t old_tsc, uint64_t curr_tsc );


static void inc_stats_init(void);
static void inc_stats_log(void);

static uint64_t tsc0;
static FILE* fd;

#define CODEL_FNAME "codel_%lu"
#define CODEL_FNAME_SIZE  (16 + 6 + 1)

#define WITH_TRACES




int lcore_master ( void* arg )
{
    LOG_INIT ( "lcore_master started\n" );

    // log current (start) time, current cycles and cycles per second
    FILE* timeFile = fopen("time_switch.csv", "w");
    if(!timeFile) {
        rte_panic("Error opening file time_switch.csv for writing!");
    }
    struct timespec current_time;
    clock_gettime(CLOCK_REALTIME, &current_time);
    fprintf(timeFile, "%lld.%09ld,", (long long) current_time.tv_sec, current_time.tv_nsec);
    fprintf(timeFile, "%lu,%lu\n", rte_get_tsc_cycles(), rte_get_tsc_hz());
    fclose(timeFile);

    inc_stats_init();

    trace_event_queue_t* entry;
    long unsigned writer_index;
    while ( !LIST_EMPTY ( &trace_event_queues_list ) ) {
        LIST_FOREACH ( entry, &trace_event_queues_list, l_entry ) {
            writer_index = entry->queue->writer_index;

            for ( ; entry->reader_index < writer_index; entry->reader_index++ ) {
                entry->logger ( entry );
            }

            inc_stats_log();

            if ( entry->reader_index >= entry->queue->size ) {
                LOG_VAR ( entry->reader_index, "%lu" );
                LOG_VAR ( entry->queue->size, "%lu" );
                fflush ( entry->fd );
                fclose ( entry->fd );
                LIST_REMOVE ( entry, l_entry );
                LOG_INIT ( "ran out of space in event queue" );
            }
        }
    }

    uint64_t tsc_hz = rte_get_tsc_hz();
    switch_stats_t old_stats = stats;
    //qelem_stats_t old_qstats = qdev_queue_stats_get(1,0);
    memcpy ( &old_stats, &stats, sizeof ( switch_stats_t ) );
    uint64_t old_tsc = rte_rdtsc();

    while ( 1 ) {
        uint64_t curr_tsc = rte_rdtsc();
        if ( curr_tsc - old_tsc > tsc_hz ) {
            switch_stats_t curr_stats;
            memcpy ( &curr_stats, &stats, sizeof ( switch_stats_t ) );
            switch_stats_print1 ( &old_stats, &curr_stats, old_tsc, curr_tsc );
            old_stats = curr_stats;


//            qelem_stats_t qurr_stats = qdev_queue_stats_get(3,0);
//            qelem_stats_print1 ( &old_qstats, &qurr_stats, old_tsc, curr_tsc );
//            old_qstats = qurr_stats;

            old_tsc = curr_tsc;

// 	    print stats from the card:
// 	    struct rte_eth_stats stats;
// 	    int ret = rte_eth_stats_get(2, &stats);
// 	    RTE_LOG ( INFO, USER5, "stats: %lu %lu %lu;\n",
// 		      stats.ierrors, stats.ibadcrc, stats.ibadlen );
//
// 	    struct rte_eth_xstats xstats[128];
// 	    ret = rte_eth_xstats_get(2, xstats, 128);
// 	    int j;
// 	    for(j = 0; j < ret; j++){
// 	      RTE_LOG ( INFO, USER5, "xstats: %s %lu\n",
// 			xstats[j].name, xstats[j].value );
// 	    }
        }
    }

    return -1;
}

int lcore_master_sighandler ( void )
{
    LOG_INIT ( "lcore_master: closing log files ...\n" );

    trace_event_queue_t* entry;
    unsigned writer_index;
    LIST_FOREACH ( entry, &trace_event_queues_list, l_entry ) {
        fflush ( entry->fd );
        fclose ( entry->fd );
    }

    exit(0);
    return -1;
}


//-------------------------------------------------------------------------------------------------------------------------
void inc_stats_init(void)
{
  time_t raw_time;
  struct tm *tm;

  time ( &raw_time );
  tm = localtime ( &raw_time );

  char time_buffer[20];
  strftime ( time_buffer,20,"%FT%X", tm );

  // open tracefile for writing:
  char filename[64];
  sprintf ( ( char* ) &filename, "%s_%s.log", "ethstats" , time_buffer );
  fd = fopen ( filename, "w" );

  if ( !fd )
  {
    rte_panic ( "error opening file '%s' for writing: %s\n", filename, strerror ( errno ) );
  }
  
  
}


void inc_stats_log(void)
{
//   const uint64_t tsc_hz = rte_get_tsc_hz();
    uint64_t time = rte_get_timer_cycles();

    struct rte_eth_stats stats0,stats1,stats2,stats3;

    int res0,res1,res2,res3;
    uint64_t i0,i1,o2;


    res0 = rte_eth_stats_get ( 0, &stats0 );
    res1 = rte_eth_stats_get ( 1, &stats1 );
    res2 = rte_eth_stats_get ( 2, &stats2 );
//    res3 = rte_eth_stats_get ( 3, &stats3 );

    i0 = stats0.ibytes; uint64_t ip0 = stats0.ierrors;
    i1 = stats1.ibytes;
    o2 = stats2.obytes;
//    o3 = stats3.opackets;

int i0rxDesc = rte_eth_rx_queue_count(0, 0); //port 0, queue 0

struct rte_eth_txq_info txQueueInfo;
rte_eth_tx_queue_info_get(2, 0, &txQueueInfo);
int o2transitqueue = 0;
for(int i = -txQueueInfo.nb_desc; i < txQueueInfo.nb_desc * 2; i++) {
    int o2transit = rte_eth_tx_descriptor_status(2, 0, i); //port 2, queue 0, first packet in the descriptors
    if(o2transit == RTE_ETH_TX_DESC_FULL) {
        o2transitqueue += 1; //prepared but not sent
    } else if(o2transit == RTE_ETH_TX_DESC_DONE) {
//        o2transitqueue = 0; //sent
    } else {
//        o2transitqueue = -1; //unavailable or error
    }
}
    fprintf ( fd, "%lu,%lu,%lu,%lu,%lu,%i,%i\n", time, i0,i1,o2,ip0,o2transitqueue,i0rxDesc );
//    fprintf ( fd, "%lu,%lu,%lu,%lu,%lu\n", time, i0,i1,i2,o3 );

}


//-------------------------------------------------------------------------------------------------------------------------

#define STAT_FIELD(field) ( ( double ) ( curr_stats->field - old_stats->field ) ) * tsc_hz /** 1e-6*/ / ( curr_tsc - old_tsc )

static
void switch_stats_print1 ( switch_stats_t* old_stats, switch_stats_t* curr_stats, uint64_t old_tsc, uint64_t curr_tsc )
{
    const uint64_t tsc_hz = rte_get_tsc_hz();
    unsigned lcore,port;
    double input_pps = 0;
    double output_pps = 0;

    for ( lcore = 0; lcore < NUM_RX_STATS; lcore++ ) {
        for ( port = 0; port < MAX_PORTS; port++ ) {
            double input_pps_lcore_port = STAT_FIELD ( rxlb_stats[lcore].rx_pkts[port] );
            RTE_LOG ( INFO, USER4, "\t lcore %u port %u:: rx %.2f Mpps\n", lcore, port, input_pps_lcore_port );
            input_pps += input_pps_lcore_port;
        }
    }

    for ( lcore = 0; lcore < NUM_TX_STATS; lcore++ ) {
        for ( port = 0; port < MAX_PORTS; port++ ) {
            double output_pps_lcore_port = STAT_FIELD ( tx_stats[lcore].tx_pkts[port] );
            RTE_LOG ( INFO, USER4, "\t lcore %u port %u:: tx %.2f Mpps\n", lcore, port, output_pps_lcore_port );
            output_pps += output_pps_lcore_port;
        }
    }
    //RTE_LOG ( INFO, USER4, "stats:: rx %.2f Mpps; free-iter(): %.2f Miter\n", input_pps, output_pps );

//   for ( lcore  = 0; lcore < NUM_LOOKUP_CORES; lcore++ )
//   {
//     double incomming_pkts = STAT_FIELD ( lookup_stats[lcore].incomming_pkts );
//     double incomming_missed_pkts = STAT_FIELD ( lookup_stats[lcore].incomming_missed_pkts );
//     double lookup_misses = STAT_FIELD ( lookup_stats[lcore].lookup_misses );
//     double key_reinstalls = STAT_FIELD ( lookup_stats[lcore].reinstalled_keys );
//     double outgoing_pkts = STAT_FIELD ( lookup_stats[lcore].outgoing_pkts );
//     RTE_LOG ( INFO, USER4, "\tlookup-core %u:: inc %.2f Mpps; inc-missed: %.2f Mpps; misses %.2f Mpps; reinstalls: %.2f Mpps; out: %.2f Mpps;\n",
//               lcore, incomming_pkts, incomming_missed_pkts, lookup_misses, key_reinstalls, outgoing_pkts );
//   }

//     unsigned pkts_in_pool = rte_mempool_count ( insatnce.pkt_pool );
//   unsigned ctrls_in_pool = rte_mempool_count ( ctrl_buf_pool );
//     rte_mempool_audit();
//     RTE_LOG ( INFO, USER4, "free objects in pkt-mempool: %u;\n", pkts_in_pool );

}

void qelem_stats_print1 ( qelem_stats_t* old_stats, qelem_stats_t* curr_stats, uint64_t old_tsc, uint64_t curr_tsc )
{
    const uint64_t tsc_hz = rte_get_tsc_hz();
    double pkts_in_pps = STAT_FIELD ( pkts_in );
    double tail_drop_pps = STAT_FIELD ( tail_drops );
    double aqm_drop_pps = STAT_FIELD ( aqm_drops );

    RTE_LOG ( INFO, USER4, "qelem stats(Kpps):: pkts_in: %.2f; taildrop: %.2f; aqm_drop: %.2f;\n", pkts_in_pps, tail_drop_pps, aqm_drop_pps );
//     RTE_LOG ( INFO, USER4, "qelem total stats:: taildrop: %lu pkts; aqm_drop: %lu pkts;\n", curr_stats->tail_drops, curr_stats->aqm_drops );
}

