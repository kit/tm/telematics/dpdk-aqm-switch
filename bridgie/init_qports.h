#ifndef	__INIT_QDEVS__
#define	__INIT_QDEVS__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
  
void init_qports(void);
  
#ifdef __cplusplus
}
#endif /* __cplusplus */  

#endif