#ifndef	CONFIG_QDEV
#define	CONFIG_QDEV

#include "def.h"

/**
 * @file port configuration for bw100gb testbed
 * 
 * must use the same name CONFIG_ETHDEV in include guard as other ethedev configurations
 */

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

// GENERAL PARAMETERS ----------------------------------------------------------------------------------------------------  

#define QLEN_PKTS (65536)
// #define RTT (DELAYx2 + 100 * 1e-6) // extra 100us delay (see ping)
#define RTT (DELAYx2) // extra 100us delay (see ping) 

#define BDP (BANDWIDTH * RTT / 8)
#define MINPKT 64
#define AVGPKT 1514

#include "qelems.h"


// QDEV CONFIG-----------------------------------------------------------------------------------------------------------

#define MIN_SEND_BURST_10g 4
#define MIN_SEND_BURST_1g 2
#define MIN_SEND_BURST_100m 1

// ethernet ports configuration; to be read in the order of (port_type_t - 1)
static const qport_conf_t qport_confs[max_port_types-1] = {
    // port port_type_normal
    {
        .min_send_burst = MIN_SEND_BURST_10g,
	.qelem_params = &qnoop_params,
	.qelem_factory = &qelem_noop_factory
    },
    // port_type_aqm
    {
        .min_send_burst = MIN_SEND_BURST_10g,
// 	.qelem_params = &qtail_bdp_params, 
// 	.qelem_params = &qtail_06bdp_params, 
// 	.qelem_params = &qtail_006bdp_params, 
    .qelem_params = &qtail_longqueue_params,
//    .qelem_params = &qtail_2bdp_params,
//	.qelem_params = &qtail_04bdp_params, 
	.qelem_factory = &qelem_taildrop_factory
//  	.qelem_params = &dgsp_params, 
//  	.qelem_params = &dgsp_params3, 
//  	.qelem_factory = &qelem_delay_gsp_factory
// 	.qelem_params = &codel_params_005, 
// 	.qelem_factory = &qelem_codel_factory
// 	.qelem_params = &pie_params, 
// 	.qelem_factory = &qelem_pie_factory
    },
    // port_type_lowspeed
    {
        .min_send_burst = MIN_SEND_BURST_1g,
	.qelem_params = &qnoop_params, 
	.qelem_factory = &qelem_noop_factory
    },
    // port_type_lowspeed_aqm
    {
        .min_send_burst = MIN_SEND_BURST_1g,
//	.qelem_params = &qtail_2bdp_params, 
    .qelem_params = &qtail_longqueue_params,
	.qelem_factory = &qelem_taildrop_factory
    }
};


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
