#ifndef	__ETHDEV_CONFIG__
#define	__ETHDEV_CONFIG__

#warning "this file is deprecated"

#include "def.h"

#ifdef __cplusplus
extern"C" {
#endif /* __cplusplus */

//-----------------------------------------------------------------------------------------------------------  

static const struct rte_eth_conf def_port_conf = {
    .rxmode = {
        .max_rx_pkt_len = ETHER_MAX_LEN,
        .split_hdr_size = 0,
        .header_split   = 0, /**< Header Split disabled */
        .hw_ip_checksum = 0, /**< IP checksum offload disabled */
        .hw_vlan_filter = 0, /**< VLAN filtering disabled */
        .jumbo_frame    = 0, /**< Jumbo Frame Support disabled */
        .hw_strip_crc   = 0, /**< CRC stripped by hardware */
// #if NUM_RX_STATS > 1
// 	.mq_mode = ETH_MQ_RX_RSS,
// #endif
    },
    .txmode = {
        .mq_mode = ETH_MQ_TX_NONE,
    },
// #if NUM_RX_STATS > 1
//     .rx_adv_conf = {
//       .rss_conf = {
// 	.rss_key = 0,
// 	.rss_key_len = 0,
// 	.rss_hf = ETH_RSS_IP,
//       }
//     }
// #endif
};

static const struct rte_eth_conf low_speed_port_conf = {
    .link_speed = ETH_LINK_SPEED_100,
    .rxmode = {
        .max_rx_pkt_len = ETHER_MAX_LEN,
        .split_hdr_size = 0,
        .header_split   = 0, /**< Header Split disabled */
        .hw_ip_checksum = 0, /**< IP checksum offload disabled */
        .hw_vlan_filter = 0, /**< VLAN filtering disabled */
        .jumbo_frame    = 0, /**< Jumbo Frame Support disabled */
        .hw_strip_crc   = 0, /**< CRC stripped by hardware */
    },
    .txmode = {
        .mq_mode = ETH_MQ_TX_NONE,
    },
};

static const struct rte_eth_rxconf rx_conf = {
    .rx_thresh = {
        .pthresh = 1,
        .hthresh = 1,
        .wthresh = 1,
    },
//     .rx_free_thresh = 32,
};

static const struct rte_eth_txconf tx_conf_i40e = {
    .tx_thresh = {
        .pthresh = 32,
        .hthresh = 0,
        .wthresh = 0,
    },
    .tx_rs_thresh  = 32,
    .tx_free_thresh  = 32,
};

static const struct rte_eth_txconf tx_conf_1g = {
    .tx_thresh = {
        .pthresh = 32,
        .hthresh = 0,
        .wthresh = 2,
    },
//     .tx_rs_thresh  = 32,
};

static const struct rte_eth_txconf tx_conf_100mb = {
    .tx_thresh = {
        .pthresh = 2,
        .hthresh = 1,
        .wthresh = 1,
    },
//     .tx_rs_thresh  = 32,
};

//-----------------------------------------------------------------------------------------------------------


typedef struct {
    // port
    unsigned port_id;
    // port config
    unsigned num_rx_queues;
    unsigned num_tx_queues;
    const struct rte_eth_conf* port_conf;
    // rx-queues-config
    unsigned num_rx_descriptors;
    const struct rte_eth_rxconf* rx_conf;
    // tx-queues-config
    unsigned num_tx_descriptors;
    const struct rte_eth_txconf* tx_conf;
} ethport_conf_t;

// what ports will be initialized
static const uint8_t num_bridge_up_ports = 4;
// static const uint8_t bridge_up_ports[] = {0, 1, 2, 3};

#define NUM_RX_DESCRIPTORS 128
#define NUM_TX_DESCRIPTORS 256 
// #define NUM_TX_DESCRIPTORS 64 // for lat eval

static const ethport_conf_t port_confs[] = {
    // port 1gb-1
//     {
//         .port_id = 0,
//         .num_rx_queues = 1,
//         .num_tx_queues = 1,
//         .port_conf = &low_speed_port_conf,
//         .num_rx_descriptors = NUM_RX_DESCRIPTORS,
//         .rx_conf = NULL,
//         .num_tx_descriptors = 32,
//         .tx_conf=&tx_conf_100mb
//     },
    // port 10gbe-0
    {
        .port_id = 1,
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .port_conf = &def_port_conf,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = NUM_TX_DESCRIPTORS,
        .tx_conf=NULL, //&tx_conf_i40e
    },
    // port 10gbe-1
    {
        .port_id = 2,
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .port_conf = &def_port_conf,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = NUM_TX_DESCRIPTORS,
        .tx_conf=NULL, //&tx_conf_i40e
    },
    // port 10gbe-2
    {
        .port_id = 3,
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .port_conf = &def_port_conf,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = NUM_TX_DESCRIPTORS,
        .tx_conf=NULL, //&tx_conf_i40e
    },
    // port 10gbe-3
    {
        .port_id = 4,
        .num_rx_queues = 1,
        .num_tx_queues = 1,
        .port_conf = &def_port_conf,
        .num_rx_descriptors = NUM_RX_DESCRIPTORS,
        .rx_conf = NULL,
        .num_tx_descriptors = NUM_TX_DESCRIPTORS,
        .tx_conf=/*NULL, //*/&tx_conf_i40e
    }
};


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
