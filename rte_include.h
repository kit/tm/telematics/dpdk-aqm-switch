#ifndef	RTE_INCLUDE_H
#define	RTE_INCLUDE_H

#include <rte_byteorder.h>
#include <rte_common.h>
#include <rte_cycles.h>
#include <rte_eal.h>
#include <rte_errno.h>
#include <rte_eth_ring.h>
#include <rte_ether.h>
#include <rte_ethdev.h>
#include <rte_hash_crc.h>
#include <rte_ip.h>
#include <rte_jhash.h>
#include <rte_lcore.h>
#include <rte_log.h>
#include <rte_malloc.h>
#include <rte_mbuf.h>
#include <rte_mempool.h>
#include <rte_meter.h>
#include <rte_pipeline.h>
#include <rte_port_ethdev.h>
#include <rte_port_ring.h>
#include <rte_port_sched.h>
#include <rte_port_source_sink.h>
#include <rte_random.h>
#include <rte_sched.h>
#include <rte_spinlock.h>
#include <rte_table_hash.h>
#include <rte_table_lpm.h>
#include <rte_table_stub.h>
#include <rte_tcp.h>
#include <rte_udp.h>

typedef struct rte_pipeline rte_pipeline_t;

#define ETHER_TYPE_IPv4 RTE_ETHER_TYPE_IPV4
#define ETHER_TYPE_IPv6 RTE_ETHER_TYPE_IPV6
#define ETHER_ADDR_LEN RTE_ETHER_ADDR_LEN
#define ETHER_MAX_LEN RTE_ETHER_MAX_LEN
#define ether_addr rte_ether_addr
#define ether_hdr rte_ether_hdr
#define ipv4_hdr rte_ipv4_hdr
#define ipv6_hdr rte_ipv6_hdr
#define tcp_hdr rte_tcp_hdr
#define udp_hdr rte_udp_hdr

#define is_broadcast_ether_addr rte_is_broadcast_ether_addr
#define is_multicast_ether_addr rte_is_multicast_ether_addr


#endif
