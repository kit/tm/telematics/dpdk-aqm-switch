
all: run-makes	

run-makes:
	cd qelems && $(MAKE) O=../build
	touch bridgie/main.c
	cd bridgie && $(MAKE) O=../build
	
clean:
	cd qelems && $(MAKE) clean O=../build
	cd bridgie && $(MAKE) clean O=../build